Milton
~~~~~~

There are a lot of CMSes out there. They all do lots of things. Some are very powerful, some are very pretty, and some are very big.

I wanted something else.

My goal with Milton is to create a small CMS suitable for small and personal sites. It has the flexibility to handle multiple domains with siloed content, several blogs per site, and a host of static pages.

Quick start
===========

1. Add "milton" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'milton',
        'taggit',
    )

2. Include the milton URLconf in your project urls.py like this::

    url(r'^milton/', include('milton.urls')),

3. Run `python manage.py migrate` to create the milton models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create some content (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/ to view the content.


Features
========

* Markdown and Textile support
* Stories (dated posts -- blog-ish or news-ish)
* Pages (static pages -- /about or /products)
* Tagging and categories
* Metaweblog XML-RPC support (with some MT and WP extensions, including categories, tags, pages, and media objects -- MarsEdit and BlogPress love it)
* RSD support for blog editors
* RSS 2.0 support
* Cache-friendly (Django's cache, nginx proxy cache, Varnish, etc.)
* URL management is central.
    Objects know their URL and when it changes they register the new URL.  This has the effect of the system tracking every URL an object has had and then redirecting users to the object's new address properly.  When an object is unavailable the redirect machinery will properly return a 410 Gone response.
* Import from Jekyll and Wordpress.
* Export to Jekyll

TODO
----

Feel free to submit PRs for these.

* Integrate the Site object with the Django Site object
* Integrate contrib.sitemap (https://docs.djangoproject.com/en/dev/ref/contrib/sitemaps/)
* RSS feeds for archive indicies (dates, sections, tags, etc. -- any archive page).
* General file attachments to resources in the Django admin instead of having to manually make a MediaObject.
* Decent default template.
* Template documentation.
* Data-level compatability with Wordpress and Jekyll for most things, and the importers/exporters to match.
* Versioned posts.
    The mechanics of this could get interesting, but when a post is changed, the old copy should be around for the sake of archiving.  It should be available to the template as well, in case I choose to publish a version history.

Never-Do
--------

These just aren't in the vision.  Feel free to fork (and rename your fork), though.

* Comments
* Workflow and collaboration
* Private posts (requires users to login; a variant of this that is find-by-url is conceivable)
