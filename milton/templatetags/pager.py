# -*- coding: utf-8 -*-

from django import template

register = template.Library()

def pager(context, fuzz=2):
    """
    Adds pagination context variables for use in displaying first, adjacent and
    last page links in addition to those created by the object_list generic
    view.
    """
    
    if 'page' in context:
        page = context['page']
    elif 'page_obj' in context:
        page = context['page_obj']
    else:
        return
    
    paginator = page.paginator
    
    page_numbers = [n for n in range(page.number - fuzz, page.number + fuzz) if n in paginator.page_range]
    
    result = {
        "results_per_page": paginator.per_page,
        "page": page.number,
        "pages": paginator.num_pages,
        "page_numbers": page_numbers,
        "has_next": page.has_next(),
        "has_previous": page.has_previous(),
        "show_first": 1 not in page_numbers,
        "show_last": paginator.count not in page_numbers,
    }
    if result['has_next']:
        result["next"] = page.next_page_number()
    if result['has_previous']:
        result['previous'] = page.previous_page_number(),
    
    return result

register.inclusion_tag("milton/pager.html", takes_context=True)(pager)
