# -*- coding: utf-8 -*-

from django import template
import re

register = template.Library()

@register.filter
def slugify(string):
    string = re.sub('\s+', '-', string)
    string = re.sub('[^\w.-]', '-', string)
    string = string.strip('_.- ').lower()
    string = re.sub('--+', '-', string)
    return string or "no-title"
