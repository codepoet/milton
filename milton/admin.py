# -*- coding: utf-8 -*-

from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from milton.models import *

## ACTIONS ##

def action_mark_draft(model_admin, request, query_set):
    query_set.update(status=STATUS_DRAFT)
action_mark_draft.short_description = "Change status of selected items to Draft"

def action_mark_published(model_admin, request, query_set):
    query_set.update(status=STATUS_PUBLISHED)
action_mark_published.short_description = "Change status of selected items to Published"

def action_open_comments(model_admin, request, query_set):
    query_set.update(allow_comments=COMMENTS_ENABLED)
action_open_comments.short_description = "Allow comments on the selected items"

def action_close_comments(model_admin, request, query_set):
    query_set.update(allow_comments=COMMENTS_CLOSED)
action_close_comments.short_description = "Close comments on the selected items"

def action_disable_comments(model_admin, request, query_set):
    query_set.update(allow_comments=COMMENTS_DISABLED)
action_disable_comments.short_description = "Disable and hide comments on the selected items"

### PAGES ###

class PageForm(forms.ModelForm):
    url = forms.RegexField(label=_("URL"), max_length=255, regex=r'^[-\w/]+$',
        help_text      = _("Example: '/about/contact/'. Make sure to have leading"
                      " and trailing slashes."),
        error_messages = {"invalid": _("This value must contain only letters, numbers,"
                          " underscores, dashes or slashes.")})
    class Meta:
        model = Page
        exclude = []


class PageAdmin(admin.ModelAdmin):
    form           = PageForm
    list_display   = ('title', 'absolute_url', 'visible', 'status', 'date_created', 'date_modified', 'date_published', 'date_hidden')
    list_editable  = ('status',)
    list_filter       = ('date_published', 'date_modified', 'status')
    date_hierarchy = 'date_published'
    search_fields  = ('title', 'url')
    actions           = [action_mark_draft, action_mark_published]
    readonly_fields = ['date_created', 'date_modified']
    fieldsets       = [
        ('',                {'fields': ['site', 'user', 'title', 'url']}),
        (_('Content'),        {'fields': ['content_format', 'content', 'template_name']}),
        (_('Availability'), {'fields': ['status', 'date_published', 'date_hidden', 'date_created', 'date_modified'], 'classes':('collapse',)}),
    ]
    
    def absolute_url(self, obj):
        return obj.get_absolute_url()
    
    def save_model(self, request, obj, form, change):
        if change is False and getattr(obj, 'user', None) is None:
            obj.user = request.user
        obj.save()
    

admin.site.register(Page, PageAdmin)

### STORIES ###

class StoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'site', 'status', 'allow_comments', 'section', 'date_published', 'date_hidden', 'date_modified')
    # list_editable = ('site', 'status', 'section')
    list_filter = ('section', 'date_published', 'date_hidden', 'date_modified', 'status', 'site')
    date_hierarchy = 'date_published'
    search_fields = ('title', '^user__username', 'slug')
    prepopulated_fields = {"slug": ("title",)}
    actions = [action_mark_draft, action_mark_published, action_open_comments, action_close_comments, action_disable_comments]
    readonly_fields = ['date_created', 'date_modified']
    fieldsets = [
        ('',                {'fields': ['site', 'user', 'title', 'slug', 'section', 'categories', 'allow_comments']}), #, 'tags']}),
        ('Content',         {'fields': ['content_format', 'excerpt', 'content']}),
        ('Availability',    {'fields': ['status', 'date_published', 'date_hidden', 'date_created', 'date_modified'], 'classes':('collapse',)}),
        ]

    def site_name(self, obj):
        return obj.site.name
    
    def save_model(self, request, obj, form, change):
        if change is False and getattr(obj, 'user', None) is None:
            obj.user = request.user
        obj.save()

admin.site.register(Story, StoryAdmin)

class ContentHistoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'content_type', 'object_id', 'field_name', 'date_created')
    list_filter = ('field_name', 'date_created')
    date_hierarchy = 'date_created'
    search_fields = ('field_name', 'content')
    readonly_fields = ['date_created', 'date_modified']
    
    def title(self, obj):
        return obj.__str__()
    title.short_description = "Item Name"
    
admin.site.register(ContentHistory, ContentHistoryAdmin)

### MEDIAOBJECTS ###

class MediaObjectAdmin(admin.ModelAdmin):
    list_display = ("filename", "content_size", "content_type", "user", "date_created")
    list_filter = ("content_type", "user")
    date_hierarchy = "date_created"
    readonly_fields = ("content_size",)
    search_fields = ("filename",)
    
    def content_size(self, obj):
        return obj.content.size
    content_size.short_description = "Content Size"
admin.site.register(MediaObject, MediaObjectAdmin)

### SECTIONS ###

class SectionAdmin(admin.ModelAdmin):
    list_display = ("name", "slug", "site")
    prepopulated_fields = {"slug": ("name",)}

admin.site.register(Section, SectionAdmin)

### CATEGORIES ###

class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "slug", "site")
    prepopulated_fields = {"slug": ("name",)}

admin.site.register(Category, CategoryAdmin)
    
### REDIRECTION ###

class RedirectAdmin(admin.ModelAdmin):
    list_display = ("original", "current", "site", "content_type", "object_id", "permanent", "date_modified", "date_created")
    fieldsets = (
        ("Source", {"fields": ["site", "original"]}),
        ("Destination", {"fields": ["current", "content_type", "object_id"]}),
        ("Options", {"fields": ["permanent"]})
    )
    list_filter = ('site__name', 'date_created', 'date_modified', 'permanent', 'content_type')
    readonly_fields = ['date_created', 'date_modified']

admin.site.register(Redirect, RedirectAdmin)

### SITE ###

class SiteAdmin(admin.ModelAdmin):
    list_display = ("name", "base_url", "alias_for")

admin.site.register(Site, SiteAdmin)
