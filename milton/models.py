# -*- coding: utf-8 -*-

import datetime
import logging
import os
import urllib.parse

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes import fields as generic
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.managers import CurrentSiteManager
from django.core.cache import cache
from django.core.files import File
from django.urls import reverse, NoReverseMatch
from django.db import models
from django.db.models import Q
from django.utils.safestring import mark_safe
from django.utils import timezone

from taggit.managers import TaggableManager

from milton.middleware.RequestMiddleware import RequestMiddleware

try:
    import markdown
except:
    markdown = None

try:
    import textile
except:
    textile = None


# REMEMBER: Django defaults all fields to null=False, blank=False. Only add
# these attributes if you wish to deviate from this behavior.

log = logging.getLogger(__name__)


### ENUMERATIONS ###

COMMENTS_DISABLED = 0
COMMENTS_CLOSED = 1
COMMENTS_ENABLED = 2

COMMENT_STATES = (
    (COMMENTS_DISABLED, 'Disabled'),
    (COMMENTS_CLOSED, 'Closed'),
    (COMMENTS_ENABLED, 'Enabled'),
)


STATUS_DRAFT = 0
STATUS_PUBLISHED = 1

STATUSES = (
    (STATUS_DRAFT, "Draft"),
    (STATUS_PUBLISHED, "Published"),
)


FORMAT_NONE = 0
FORMAT_TEXTILE = 1
FORMAT_MARKDOWN = 2

CONTENT_FORMATTERS = (
    (FORMAT_NONE, "None"),
    (FORMAT_TEXTILE, "Textile"),
    (FORMAT_MARKDOWN, "Markdown"),
)


### GLOBAL FUNCTIONS ###

def format_text(text_format, text):
    output = text
    if text_format == FORMAT_TEXTILE:
        if textile != None:
            output = textile.textile(text)
        else:
            log.info("No Textile support found.")
    elif text_format == FORMAT_MARKDOWN:
        if markdown != None:
            output = markdown.markdown(text)
        else:
            log.info("No Markdown support found.")
    return mark_safe(output)


### MANAGERS ###

class MiltonCurrentSiteManager (CurrentSiteManager):
    def get_queryset(self):
        return super(CurrentSiteManager, self).get_queryset().filter(
            **{self._get_field_name() + '__id': Site.objects.get_current().id}
        )

class PublishedManager (MiltonCurrentSiteManager):
    def get_queryset(self):
        qs = super(PublishedManager,self).get_queryset()
        # qs = qs.filter(site__id__exact=Site.objects.get_current().id)
        qs = qs.filter(status = STATUS_PUBLISHED, date_published__lte=timezone.now())
        qs = qs.filter( Q(date_hidden=None) | Q(date_hidden__gt=timezone.now()))
        return qs

class SiteManager (models.Manager):
    def get_current(self):
        site = None
        request = RequestMiddleware.current_request()
        
        # See if we've fetched this Site for this request before.
        if hasattr(request, 'Site'):
            return request.Site
        
        # Look for a matching site
        if request:
            site = self.get_site_for_URL(request.build_absolute_uri())
        
        # Use the Site with SITE_ID as a fallback.
        if not site:
            site = self.get_queryset().get(pk=settings.SITE_ID)
        
        # Cache the fetched Site object in the request.
        if request:
            setattr(request, 'Site', site)
        
        return site

    def get_site_for_URL(self, url):
        '''
        Looks up a Site object for a given URL.  Caches the results for
        a short time to avoid destroying lesser databases.
        
    Input: http://bob.blogs.public.example.com/blogs/bob/ted/sam/index.html?count=1&path=/some/data&interesting
    Hostname: bob.blogs.public.example.com
    Path: /blogs/bob/ted/sam
    Result: [
            'bob.blogs.public.example.com',
            'blogs.public.example.com',
            'public.example.com',
            'example.com',
         ]
        '''
        parts = urllib.parse.urlparse(url)
        hostname = parts.hostname
        
        if len(hostname) == 0:
            return None
        
        # Check the cache for a recent answer.
        cache_key = "milton.site:%s" % (hostname,)
        site = cache.get(cache_key)
        if site:
            if site == "None":
                return None
            return site
        
        # Generate all valid permutations of the hostname.
        permutations = ['']
        hostnames = []
        
        #FIXME: If the hostname is an IP, we shouldn't split it the same way as a DNS name.
        
        hostname_parts = hostname.split(".") # ["bob", "example", "com"]
        
        while len(hostname_parts) > 0:
            hostnames.append(".".join(hostname_parts))
            hostname_parts.pop(0)
        
        permutations.extend(hostnames)
        permutations.sort(key=len, reverse=True)
        
        # Find the best matching site.
        query = self.get_queryset().filter(base_url__in=permutations)
        best_choice = None
        for site in query:
            if not best_choice:
                best_choice = site
                continue
            
            if len(site.base_url) > len(best_choice.base_url):
                best_choice = site
        
        if best_choice:
            cache.set(cache_key, best_choice, 5)
        else:
            cache.set(cache_key, "None", 5)
        
        return best_choice


### SPObject ###

class SPObject (models.Model):
    date_created = models.DateTimeField("Date Created", editable=False, default=timezone.now)
    date_modified = models.DateTimeField("Date Modified", editable=False, default=timezone.now)
    
    class Meta:
        abstract = True
    
    def __init__(self, *args, **kwargs):
        super(SPObject, self).__init__(*args, **kwargs)
        self._original_state = self._as_dict()
    
    def _as_dict(self):
        '''
        Returns the values of non-relationship model objects in a dictionary.
        '''
        return dict([(f.name, getattr(self, f.name)) for f in self._meta.local_fields if not f.remote_field])
    
    def get_changed_fields(self):
        '''
        Returns a dictionary where the key is the field that changed and the value is the original value.
        '''
        new_state = self._as_dict()
        return dict([(key, value) for key, value in list(self._original_state.items()) if value != new_state[key]])
    
    def is_dirty_field(self, field):
        ''' Returns True if a field has been changed since the object was last saved. '''
        try:
            return self.__dict__.get(field, None) != self._original_state[field]
        except KeyError:
            return True
    
    def save(self, *args, **kwargs):
        if not self.date_created:
            self.date_created = timezone.now()
        
        ''' If the modification date was either not set or NOT manually changed since we were made, update it. '''
        if not self.date_modified or not self.is_dirty_field('date_modified'):
            self.date_modified = timezone.now()
                
        super(SPObject, self).save(*args, **kwargs)
        
        self._original_state = self._as_dict()

class MediaObject(SPObject):
    filename = models.CharField(max_length=255)
    content_type = models.CharField(max_length=255)
    content = models.FileField()
    user = models.ForeignKey(User, related_name="media_objects", on_delete=models.CASCADE)
    
    def get_absolute_url(self):
        return self.content.url

### Model Objects ###

class Site(SPObject):
    '''
    The Base URL is special.  It works similar to Drupal's concept of sites in that several variations are generated, all are searched against, and the longest match wins (so that we can have one DB query).
    http://mysite.bob.example.com/blog/bob/ will look for:
        mysite.bob.example.com
        bob.example.com
        example.com
        [empty]
    Yes, an empty string is valid as a default site or a single-site installation with multiple domain names. Note that paths are not searched.
    '''
    base_url = models.CharField("Base URL", max_length=255, blank=True, unique=True, help_text="An installation-wide unique value that will match against the host in the request URL and uniquely identifies this site (eg. example.com or bob.example.com). This value is also used to compose permalinks.")
    
    name = models.CharField("Name", max_length=255, blank=True, help_text="The human-readable, publicly-displayed name of the site.")
    slogan = models.CharField("Slogan", max_length=255, blank=True, help_text="A short description or slogan for the site; a subhead.")
    description_html = models.TextField("Description HTML", blank=True, help_text="This will be passed to templates as-is.")
    footer_html = models.TextField("Footer HTML", blank=True, help_text="This will be passed to templates as-is.")
    
    alias_for = models.ForeignKey('Site', null=True, blank=True, related_name="aliases", help_text="When this site is matched, use the site indicated here instead. Site aliases will never show their own content.", on_delete=models.CASCADE)
    
    objects = SiteManager()
    
    class Meta:
        verbose_name = "site"

    def __str__(self):
        return self.name


class Section(SPObject):
    # TODO: Create admin class that auto-gens the slug
    name = models.CharField("Section Name", max_length=255)
    slug = models.SlugField("Section Slug", max_length=255)

    # page = models.ForeignKey(Page, null=True, blank=True, help_text="A page to display instead of a section index.") # TODO: Would love this...
    site = models.ForeignKey(Site, default=1, related_name="sections", help_text='The site(s) this item is accessible at.', on_delete=models.CASCADE)
    
    objects = models.Manager()
    on_site = MiltonCurrentSiteManager()
    
    class Meta:
        verbose_name = "Section"
        verbose_name_plural = "Sections"
    
    def __str__(self):
        if self.site:
            site_name = self.site.name
        else:
            site_name = "global"
        return "%s (%s)" % (self.name, site_name)
    
    # @models.permalink
    def get_absolute_url(self):
        return reverse('story-list', kwargs = {'section':self.slug})


### CATEGORIES/TAXONOMY ###

class Category(SPObject):
    """ A category. """
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50)
    description = models.TextField("Description", blank=True, help_text="Displayed to end-users when showing the index.")
    
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    site = models.ForeignKey(Site, default=1, related_name="categories", help_text='The site(s) this item is accessible at.', on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"
    
    def __str__(self):
        if self.site:
            site_name = self.site.name
        else:
            site_name = "global"
        return "%s (%s)" % (self.name, site_name)
    
    def natural_key(self):
        return self.slug
    
    def get_by_natural_key(self, slug):
        return self.get(slug=slug)

# TODO: Break content out into a 1:1 object so it can be versioned. Change all variable-length fields to fixed-length to aid in faster lookups on this table.
class Resource(SPObject):
    # Properties
    title = models.CharField("Title", max_length=255)
    content_format = models.PositiveIntegerField("Content Format", choices=CONTENT_FORMATTERS, default=FORMAT_MARKDOWN, help_text="The formatter the content (and excerpt) should be run through when rendering.")
    excerpt = models.TextField("Excerpt", blank=True, help_text="If populated, the text to display on archive pages instead of the content.")
    content = models.TextField("Content", blank=True, help_text='The content to display on the page.')
    
    # Publishing status
    status = models.IntegerField("Status", choices=STATUSES, default=STATUS_PUBLISHED, help_text="Only published items will be visible on the site.")
    date_published = models.DateTimeField("Date Published", default=timezone.now, help_text="Item will become visible after this date.  Future posting is supported.")
    date_hidden = models.DateTimeField("Date Hidden", null=True, blank=True, help_text="Item will be hidden past this date.  No value indicates a perpetual item (most common).")
    
    # Meta display
    show_metadata = models.BooleanField("Show Metadata", default=True, help_text="Toggles the display of author and date information.")
    allow_comments = models.IntegerField("Comments", choices=COMMENT_STATES, default=COMMENTS_DISABLED, help_text="If comments are disabled then existing comments will be hidden.  Choose 'Closed' to display existing comments and prevent new comments from being added.")
    
    # Relationships    
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    site = models.ForeignKey(Site, default=1, help_text='The site this item is accessible at.', on_delete=models.CASCADE)
    
    # Object managers
    objects = models.Manager()
    on_site = MiltonCurrentSiteManager()
    published = PublishedManager()
    
    class Meta:
        abstract = True
        get_latest_by = 'date_published'
        index_together = ['site', 'status', 'date_published', 'date_hidden']
    
    def __init__(self, *args, **kwargs):
        super(Resource, self).__init__(*args, **kwargs)
        if self.id != None:
            try:
                self._original_url = self.get_absolute_url()
            except NoReverseMatch as e:
                self._original_url = ""
                log.error("%s ID %d failed to generate its URL!" % (self.__class__, self.pk))
        else:
            self._original_url = ""
    
    def __str__(self):
        return self.title
    
    @property
    def visible(self):
        return (self.status == STATUS_PUBLISHED and self.date_published < timezone.now())
    
    @property
    def modified_since_publication(self):
        # Allow for some immediate changes for formatting, etc.
        forgiveness = datetime.timedelta(minutes=1)
        
        return (self.date_modified > (self.date_published + forgiveness))
    
    @property
    def formatted_excerpt(self):
        if self.excerpt and len(self.excerpt):
            return format_text(self.content_format, self.excerpt)
        else:
            return self.formatted_content
    
    @property
    def formatted_content(self):
        return format_text(self.content_format, self.content)
    
    @property
    def previous(self):
        query = Story.published.order_by('-date_published', 'id').filter(date_published__lt=self.date_published)[0:1]
        if len(query):
            return query[0]
        else:
            return None
    
    @property
    def next(self):
        query = Story.published.order_by('date_published', 'id').filter(date_published__gt=self.date_published)[0:1]
        if len(query):
            return query[0]
        else:
            return None
    
    def save(self, *args, **kwargs):
        update_excerpt_history = (self.excerpt and self.is_dirty_field("excerpt"))
        update_content_history = (self.content and self.is_dirty_field("content"))
        
        super(Resource, self).save(*args, **kwargs)
        
        '''
        Create a redirect with the current URL if the path has changed.
        We use the current URL so that if this object is ever deleted or hidden, we can display a 410/Gone message.
        Since we create these from the start of life for the object, all previous URLs will be recorded EXCEPT
        for the case when two objects will have shared the same URL at some point, at which case the original
        object wins (in the automated fashion; admins can always update the Redirect object manually).
        '''
        if self._original_url != self.get_absolute_url():
            (r, c) = Redirect.objects.get_or_create(
                original = self.get_absolute_url(),
                site = self.site,
                defaults = {
                    'target_object': self,
                    'permanent': True
                }
            )
            if c:
                r.save()
            self._original_url = self.get_absolute_url()
        
        if update_excerpt_history:
            ch = ContentHistory(owner=self, field_name="excerpt", content=self.excerpt)
            ch.save()
        
        if update_content_history:
            ch = ContentHistory(owner=self, field_name="content", content=self.content)
            ch.save()


class Page(Resource):
    url = models.CharField("URL", max_length=255, db_index=True, help_text="The URL for this page. Ensure it begins and ends with a slash.")
    template_name = models.CharField("Template", max_length=255, blank=True, help_text="If specified, this template will be used instead of 'page.html'")

    class Meta:
        ordering = ('url',)
        verbose_name = "page"
        verbose_name_plural = "pages"
        get_latest_by = 'date_published'
        unique_together = ( ('site', 'date_published', 'url') )
    
    def __str__(self):
        return "%s; %s" % (self.url, self.title)

    def get_absolute_url(self):
        return self.url


class Story(Resource):
    slug = models.SlugField("URL Slug", max_length=255)
    link = models.CharField("Link", max_length=4096, blank=True, help_text="The link this story is about, if applicable.")
    
    section = models.ForeignKey(Section, null=True, blank=True, related_name="stories", help_text="The section of the site this story will reside in.", on_delete=models.CASCADE)
    tags = TaggableManager(blank=True)
    categories = models.ManyToManyField(Category, blank=True, related_name="stories")
    
    class Meta:
        verbose_name = "story"
        verbose_name_plural = "stories"
        get_latest_by = 'date_published'
        ordering = ['-date_published', '-date_modified']
        unique_together = ( ('site', 'date_published', 'slug') )
        index_together = (
            ('id', 'user'),  # For the XML-RPC API.
        )
    
    def show_comments(self):
        return self.allow_comments != COMMENTS_DISABLED
    
    def allow_commenting(self):
        return self.allow_comments == COMMENTS_ENABLED
    
    def get_all_urls(self):
        urls = [];
        
        if self.section:
            section = self.section.slug
        else:
            section = None
        
        kwargs = {
            'section': section,
            'year':    self.date_published.year,
            'month':   "%02d"%self.date_published.month,
            'day':     "%02d"%self.date_published.day,
            'slug':    self.slug,
        }
        
        urls.append(reverse('story-detail', kwargs = kwargs))
        
        del kwargs['slug']
        urls.append(reverse('story-list', kwargs = kwargs))

        del kwargs['day']
        urls.append(reverse('story-list', kwargs = kwargs))
        
        del kwargs['month']
        urls.append(reverse('story-list', kwargs = kwargs))
        
        del kwargs['year']
        urls.append(reverse('story-list', kwargs = kwargs))
        
        urls.append('/')
        
        return urls
        
    # @models.permalink
    def get_absolute_url(self):
        kwargs = {
            'year':    self.date_published.year,
            'month':   "%02d"%self.date_published.month,
            'day':     "%02d"%self.date_published.day,
            'slug':    self.slug,
        }
        
        if self.section and len(self.section.slug):
            kwargs['section'] = self.section.slug
        
        return reverse('story-detail', kwargs = kwargs)

### REDIRECTION ###

class Redirect(SPObject):
    site = models.ForeignKey(Site, default=1, help_text='The site this redirect is active for.', on_delete=models.CASCADE)
    original = models.CharField("Original path", max_length=255, db_index=True,
        help_text='The original path for this resource.  This must be an absolute path starting from the root of the site.')
    current = models.CharField("Current path", max_length=255, blank=True,
        help_text='The current path to this resource, if it is not an object.')
    permanent = models.BooleanField(default=True, help_text='Is this redirect permanent (301)?')
    
    content_type = models.ForeignKey(ContentType, null=True, blank=True, help_text='If this redirect is to an object, select an object type.', on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField("Object ID", null=True, blank=True, help_text='The ID of the object this redirect will point to.')
    target_object = generic.GenericForeignKey()

    objects = models.Manager()
    on_site = MiltonCurrentSiteManager()
    
    class Meta:
        verbose_name = "redirect"
        verbose_name_plural = "redirects"
        ordering = ['-date_created']
        unique_together = ("site", "original")
    
    def __str__(self):
        return "%s to (%s, %s)" % (self.original, self.current, self.target_object)
    
    def destination(self):
        destination = None
        if self.current and len(self.current):
            destination = self.current
        elif self.target_object != None:
            try:
                destination = self.target_object.get_absolute_url()
            except Exception as e:
                pass
        return destination


### HISTORY ###

class ContentHistory(SPObject):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    owner = generic.GenericForeignKey()
    field_name = models.CharField(max_length=255, blank=True)
    content = models.TextField("Content", blank=True)
    
    class Meta:
        verbose_name = "content history"
        verbose_name_plural = "content histories"
        ordering = ['-date_created', 'content_type', 'object_id']
    
    def __str__(self):
        return "%s: %s" % (self.owner, self.field_name)
    
    @classmethod
    def history(owner, field):
        #FIXME: owner won't work here, need to search by ID and PK
        return ContentHistory.objects.filter(owner=owner, field_name=field).order_by('-date_created')
