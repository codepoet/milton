# -*- coding: utf-8 -*-

from logging import getLogger

from django.core.cache import cache

from milton.models import Page, Site
from milton.views import PageDetail

logger = getLogger(__name__)

class PageMiddleware(object):
    def __init__(self, get_response=None):
        self.get_response = get_response
    
    def __call__(self, request):
        path = request.path_info
        page = None
        
        # Check for cached answers.
        cache_key = "milton.page:%d:%s" % (Site.objects.get_current().id, path)
        page = cache.get(cache_key)
        if page == None:
            # Look for a Page that matches; pass on failure.
            try:
                logger.debug("PageMiddleware: Looking for a page for %s" % (path,))
                page = Page.published.get(url=path)
            except Page.DoesNotExist as e:
                cache.set(cache_key, False, 5)
        
        if page is None or page is False:
            return self.get_response(request)
        
        logger.debug("PageMiddleware: Found for a page for %s" % (path,))
        cache.set(cache_key, True, 5)
        
        response = PageDetail.as_view()(request, path)
        response.render()
        return response
