# -*- coding: utf-8 -*-

from logging import getLogger

from django.conf import settings
from django.http import HttpResponsePermanentRedirect, HttpResponseRedirect, HttpResponseGone, Http404
from django.template import RequestContext, loader

from milton.models import Redirect

logger = getLogger(__name__)

class RedirectionMiddleware(object):
    def __init__(self, get_response=None):
        self.get_response = get_response
    
    def __call__(self, request):
        response = self.get_response(request)
        
        # If the response found something, return it.
        if response.status_code != 404:
            return response
        try:
            # Look for an exact redirect
            path = request.path
            paths = [path]
            
            logger.debug("Redirect: Looking for %s" % (path,))

            r = Redirect.objects.get(site__id__exact=settings.SITE_ID, original__in=paths)
            
            # If the destination is not visible, let the 404 shine.
            if hasattr(r.target_object, 'visible') and r.target_object.visible == False:
                logger.debug("Redirect: Object exists, but is unpublished.")
                return response
            
            # If there's a redirect, process it
            destination = r.destination()
            
            # If we're about to tell someone to go where we already are, stop and think...
            if destination == request.path_info:
                return response
            
            if destination == None:
                # No destination means it's been removed, so make it a dead end.
                context = RequestContext(request, {
                    'title':'410 Gone',
                    'content':'the resource you seek / has been taken by time / seek answers within'
                })
                template = loader.select_template(["milton/410.html", "milton/error.html"])
                return HttpResponseGone(context)
            
            if r.permanent == True:
                # 301 Moved
                return HttpResponsePermanentRedirect(destination)
            else:
                # 302 Found
                return HttpResponseRedirect(destination)
        
        except Redirect.DoesNotExist as e:
            # Otherwise, return the original response
            logger.debug("Redirect: No redirect found for for %s" % (path,))
            return response
        except:
            if settings.DEBUG:
                raise
        # If we're here, we didn't catch something -- just pass along the 404.
        logger.debug("Redirect: No alternate plan for %s; passing along the NotFound." % (path,))
        return response
