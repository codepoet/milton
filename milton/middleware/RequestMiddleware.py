# -*- coding: utf-8 -*-

import threading

class RequestMiddleware(object):
    local = threading.local()
    
    def __init__(self, get_response=None):
        self.get_response = get_response
        self.local.request = None
    
    def __call__(self, request):
        self.local.request = request
        response = self.get_response(request)
        return response
    
    @classmethod
    def current_request(cls):
        try:
            return self.local.request
        except:
            return None
