# -*- coding: utf-8 -*-

from django.urls import reverse

from milton.models import Category, Section, Site

def general(request):
    rsd_url = reverse("api-rsd")
    return {
        "site": Site.objects.get_current(),
        "sections": Section.objects.filter(site=Site.objects.get_current()),
        "categories": Category.objects.filter(site=Site.objects.get_current()),
        "rsd_link": '<link rel="EditURI" type="application/rsd+xml" href="%s" />' % rsd_url
    }