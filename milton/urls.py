# -*- coding: utf-8 -*-

from django.conf import settings
from django.urls import re_path, include
from django.conf.urls.static import static

from milton.views import MainSiteFeed, StoryList, StoryDetail, StoryListByTag

# Enter the regular expressions once.
section_re = r'(?P<section>[^0-9][^/]*)'
year_re = r'(?P<year>\d{4})'
month_re = r'(?P<month>\d{1,2})'
day_re = r'(?P<day>\d{1,2})'
slug_re = r'(?P<slug>.+)'

urlpatterns = [
    # APIs
    re_path(r'_api/', include('milton.api.urls')),
    
    # Feeds
    re_path(r'^feeds/rss.xml$',
        MainSiteFeed(),
        name='site-feed'
    ),
    
    # Articles
    re_path('^(?:%s/)?%s/%s/%s/%s/$' % (section_re, year_re, month_re, day_re, slug_re),
        StoryDetail.as_view(),
        name='story-detail'
    ),
    re_path("^(?:%s/)?(?:%s/)?(?:%s/)?(?:%s/)?$" % (section_re, year_re, month_re, day_re),
        StoryList.as_view(),
        name='story-list'
    ),
    
    # Tags
    re_path(r'^tag/(?P<tag_slug>.+)/$',
        StoryListByTag.as_view(),
        name='tag'
    ),
]

# Serve media files in development
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
