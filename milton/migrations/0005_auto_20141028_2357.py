# -*- coding: utf-8 -*-

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('milton', '0004_move_fields_to_resource'),
    ]

    operations = [
        migrations.AlterField(
            model_name='site',
            name='base_url',
            field=models.CharField(verbose_name='Base URL', max_length=255, blank=True, help_text='An installation-wide unique value that will match against the host in the request URL and uniquely identifies this site (eg. example.com or bob.example.com). This value is also used to compose permalinks.', unique=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='site',
            name='name',
            field=models.CharField(verbose_name='Name', max_length=255, help_text='The human-readable, publicly-displayed name of the site.', blank=True),
            preserve_default=True,
        ),
    ]
