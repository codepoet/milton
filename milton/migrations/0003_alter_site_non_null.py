# -*- coding: utf-8 -*-

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('milton', '0002_initialdata'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='site',
            field=models.ForeignKey(help_text='The site(s) this item is accessible at.', default=1, related_name='categories', to='milton.Site', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='page',
            name='site',
            field=models.ForeignKey(help_text='The site this item is accessible at.', default=1, to='milton.Site', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='redirect',
            name='site',
            field=models.ForeignKey(help_text='The site this redirect is active for.', default=1, to='milton.Site', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='section',
            name='site',
            field=models.ForeignKey(help_text='The site(s) this item is accessible at.', default=1, related_name='sections', to='milton.Site', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='story',
            name='site',
            field=models.ForeignKey(help_text='The site this item is accessible at.', default=1, to='milton.Site', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='story',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
