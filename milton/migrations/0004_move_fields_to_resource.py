# -*- coding: utf-8 -*-

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('milton', '0003_alter_site_non_null'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='allow_comments',
            field=models.IntegerField(help_text="If comments are disabled then existing comments will be hidden.  Choose 'Closed' to display existing comments and prevent new comments from being added.", default=0, verbose_name='Comments', choices=[(0, 'Disabled'), (1, 'Closed'), (2, 'Enabled')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='page',
            name='show_metadata',
            field=models.BooleanField(help_text='Toggles the display of author and date information.', default=True, verbose_name='Show Metadata'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='page',
            name='user',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
            preserve_default=False,
        ),
    ]
