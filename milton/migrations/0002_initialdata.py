# -*- coding: utf-8 -*-

from django.db import models, migrations
from django.utils.timezone import now

def milton_generate_default_site(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    Site = apps.get_model("milton", "Site")
    db_alias = schema_editor.connection.alias
    Site.objects.using(db_alias).bulk_create([
        Site(
            id=1,
            name="Default",
            date_created=now(),
            date_modified=now(),
            base_url=""
        ),
    ])


class Migration(migrations.Migration):
    dependencies = [
        ('milton', '0001_initial'),
    ]
    operations = [
        migrations.RunPython(
            code=milton_generate_default_site,
            reverse_code=None,
            atomic=True,
        ),
    ]
