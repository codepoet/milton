# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.manager
import milton.models


class Migration(migrations.Migration):

    dependencies = [
        ('milton', '0005_auto_20141028_2357'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='page',
            managers=[
                ('objects', django.db.models.manager.Manager()),
                ('on_site', milton.models.MiltonCurrentSiteManager()),
                ('published', milton.models.PublishedManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='redirect',
            managers=[
                ('objects', django.db.models.manager.Manager()),
                ('on_site', milton.models.MiltonCurrentSiteManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='section',
            managers=[
                ('objects', django.db.models.manager.Manager()),
                ('on_site', milton.models.MiltonCurrentSiteManager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='story',
            managers=[
                ('objects', django.db.models.manager.Manager()),
                ('on_site', milton.models.MiltonCurrentSiteManager()),
                ('published', milton.models.PublishedManager()),
            ],
        ),
        migrations.AlterField(
            model_name='story',
            name='categories',
            field=models.ManyToManyField(blank=True, related_name='stories', to='milton.Category'),
        ),
    ]
