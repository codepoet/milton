# -*- coding: utf-8 -*-

from django.db import models, migrations
import django.utils.timezone
import taggit.managers
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('taggit', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Created', editable=False)),
                ('date_modified', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Modified', editable=False)),
                ('name', models.CharField(max_length=50)),
                ('slug', models.SlugField()),
                ('description', models.TextField(blank=True, help_text='Displayed to end-users when showing the index.', verbose_name='Description')),
                ('parent', models.ForeignKey(to='milton.Category', blank=True, related_name='children', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name_plural': 'Categories',
                'verbose_name': 'Category',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContentHistory',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Created', editable=False)),
                ('date_modified', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Modified', editable=False)),
                ('object_id', models.PositiveIntegerField()),
                ('field_name', models.CharField(blank=True, max_length=255)),
                ('content', models.TextField(blank=True, verbose_name='Content')),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name_plural': 'content histories',
                'ordering': ['-date_created', 'content_type', 'object_id'],
                'verbose_name': 'content history',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MediaObject',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Created', editable=False)),
                ('date_modified', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Modified', editable=False)),
                ('filename', models.CharField(max_length=255)),
                ('content_type', models.CharField(max_length=255)),
                ('content', models.FileField(upload_to='')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='media_objects', on_delete=models.CASCADE)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Created', editable=False)),
                ('date_modified', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Modified', editable=False)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('content_format', models.PositiveIntegerField(default=2, help_text='The formatter the content (and excerpt) should be run through when rendering.', verbose_name='Content Format', choices=[(0, 'None'), (1, 'Textile'), (2, 'Markdown')])),
                ('excerpt', models.TextField(blank=True, help_text='If populated, the text to display on archive pages instead of the content.', verbose_name='Excerpt')),
                ('content', models.TextField(blank=True, help_text='The content to display on the page.', verbose_name='Content')),
                ('status', models.IntegerField(default=1, help_text='Only published items will be visible on the site.', verbose_name='Status', choices=[(0, 'Draft'), (1, 'Published')])),
                ('date_published', models.DateTimeField(default=django.utils.timezone.now, help_text='Item will become visible after this date.  Future posting is supported.', verbose_name='Date Published')),
                ('date_hidden', models.DateTimeField(blank=True, help_text='Item will be hidden past this date.  No value indicates a perpetual item (most common).', verbose_name='Date Hidden', null=True)),
                ('url', models.CharField(db_index=True, max_length=255, help_text='The URL for this page. Ensure it begins and ends with a slash.', verbose_name='URL')),
                ('template_name', models.CharField(blank=True, max_length=255, help_text="If specified, this template will be used instead of 'page.html'", verbose_name='Template')),
            ],
            options={
                'verbose_name_plural': 'pages',
                'get_latest_by': 'date_published',
                'verbose_name': 'page',
                'ordering': ('url',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Redirect',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Created', editable=False)),
                ('date_modified', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Modified', editable=False)),
                ('original', models.CharField(db_index=True, max_length=255, help_text='The original path for this resource.  This must be an absolute path starting from the root of the site.', verbose_name='Original path')),
                ('current', models.CharField(blank=True, max_length=255, help_text='The current path to this resource, if it is not an object.', verbose_name='Current path')),
                ('permanent', models.BooleanField(default=True, help_text='Is this redirect permanent (301)?')),
                ('object_id', models.PositiveIntegerField(blank=True, help_text='The ID of the object this redirect will point to.', verbose_name='Object ID', null=True)),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType', help_text='If this redirect is to an object, select an object type.', blank=True, null=True, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name_plural': 'redirects',
                'verbose_name': 'redirect',
                'ordering': ['-date_created'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Created', editable=False)),
                ('date_modified', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Modified', editable=False)),
                ('name', models.CharField(max_length=255, verbose_name='Section Name')),
                ('slug', models.SlugField(max_length=255, verbose_name='Section Slug')),
            ],
            options={
                'verbose_name_plural': 'Sections',
                'verbose_name': 'Section',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Site',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Created', editable=False)),
                ('date_modified', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Modified', editable=False)),
                ('base_url', models.CharField(blank=True, max_length=255, unique=True, help_text='An installation-wide unique value that will match against the request URL and uniquely identifies this site (eg. http://example.com/bob or http://bob.example.com). This value is also used to compose permalinks. See documentation for full details.', verbose_name='Base URL')),
                ('name', models.CharField(blank=True, max_length=255, help_text='The human-readable, publicly-displayed name of the site.', verbose_name='Site Name')),
                ('slogan', models.CharField(blank=True, max_length=255, help_text='A short description or slogan for the site; a subhead.', verbose_name='Slogan')),
                ('description_html', models.TextField(blank=True, help_text='This will be passed to templates as-is.', verbose_name='Description HTML')),
                ('footer_html', models.TextField(blank=True, help_text='This will be passed to templates as-is.', verbose_name='Footer HTML')),
                ('alias_for', models.ForeignKey(to='milton.Site', help_text='When this site is matched, use the site indicated here instead. Site aliases will never show their own content.', blank=True, related_name='aliases', null=True, on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name': 'site',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Story',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Created', editable=False)),
                ('date_modified', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date Modified', editable=False)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('content_format', models.PositiveIntegerField(default=2, help_text='The formatter the content (and excerpt) should be run through when rendering.', verbose_name='Content Format', choices=[(0, 'None'), (1, 'Textile'), (2, 'Markdown')])),
                ('excerpt', models.TextField(blank=True, help_text='If populated, the text to display on archive pages instead of the content.', verbose_name='Excerpt')),
                ('content', models.TextField(blank=True, help_text='The content to display on the page.', verbose_name='Content')),
                ('status', models.IntegerField(default=1, help_text='Only published items will be visible on the site.', verbose_name='Status', choices=[(0, 'Draft'), (1, 'Published')])),
                ('date_published', models.DateTimeField(default=django.utils.timezone.now, help_text='Item will become visible after this date.  Future posting is supported.', verbose_name='Date Published')),
                ('date_hidden', models.DateTimeField(blank=True, help_text='Item will be hidden past this date.  No value indicates a perpetual item (most common).', verbose_name='Date Hidden', null=True)),
                ('slug', models.SlugField(max_length=255, verbose_name='URL Slug')),
                ('link', models.CharField(blank=True, max_length=4096, help_text='The link this story is about, if applicable.', verbose_name='Link')),
                ('show_metadata', models.BooleanField(default=True, help_text='Toggles the display of author and date information.', verbose_name='Show Metadata')),
                ('allow_comments', models.IntegerField(default=0, help_text="If comments are disabled then existing comments will be hidden.  Choose 'Closed' to display existing comments and prevent new comments from being added.", verbose_name='Comments', choices=[(0, 'Disabled'), (1, 'Closed'), (2, 'Enabled')])),
                ('categories', models.ManyToManyField(blank=True, to='milton.Category', related_name='stories', null=True)),
                ('section', models.ForeignKey(to='milton.Section', help_text='The section of the site this story will reside in.', blank=True, related_name='stories', null=True, on_delete=models.CASCADE)),
                ('site', models.ForeignKey(to='milton.Site', help_text='The site this item is accessible at. Set to no value to use on all sites (eg. a privacy policy or TOS).', blank=True, null=True, on_delete=models.CASCADE)),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', help_text='A comma-separated list of tags.', verbose_name='Tags', blank=True, through='taggit.TaggedItem')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='stories', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name_plural': 'stories',
                'verbose_name': 'story',
                'get_latest_by': 'date_published',
                'ordering': ['-date_published', '-date_modified'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='story',
            unique_together=set([('site', 'date_published', 'slug')]),
        ),
        migrations.AlterIndexTogether(
            name='story',
            index_together=set([('id', 'user')]),
        ),
        migrations.AddField(
            model_name='section',
            name='site',
            field=models.ForeignKey(to='milton.Site', help_text='The site(s) this item is accessible at. Set to no value to use on all sites.', blank=True, related_name='sections', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='redirect',
            name='site',
            field=models.ForeignKey(to='milton.Site', help_text='The site this redirect is active for. Set to no value to use on all sites.', blank=True, null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='redirect',
            unique_together=set([('site', 'original')]),
        ),
        migrations.AddField(
            model_name='page',
            name='site',
            field=models.ForeignKey(to='milton.Site', help_text='The site this item is accessible at. Set to no value to use on all sites (eg. a privacy policy or TOS).', blank=True, null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='page',
            unique_together=set([('site', 'date_published', 'url')]),
        ),
        migrations.AddField(
            model_name='category',
            name='site',
            field=models.ForeignKey(to='milton.Site', help_text='The site(s) this item is accessible at. Set to no value to use on all sites.', blank=True, related_name='categories', null=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
