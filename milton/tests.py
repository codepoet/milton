#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
from django.test import TestCase
from django.conf import settings

from milton.models import *

class PageTests(TestCase):
    site = None
    user = None
    
    def setUp(self):
        self.site = Site.objects.get_current()
        self.user = User.objects.create_user(username='testuser', password='12345')
    
    def testContentFormattingNone(self):
        obj = Page(title="Format Test", url="/format-test", user=self.user, site=self.site)
        obj.content = "*None* _test_ <b>woo!</b>"
        obj.content_format = FORMAT_NONE
        self.assertEqual(obj.formatted_content, obj.content, "Invalid result: "+obj.formatted_content)
    
    def testContentFormattingTextile(self):
        obj = Page(title="Format Test", url="/format-test", user=self.user, site=self.site)
        obj.content = "p(foo). *Textile* _test_"
        obj.content_format = FORMAT_TEXTILE
        self.assertEqual(obj.formatted_content, '\t<p class="foo"><strong>Textile</strong> <em>test</em></p>', "Invalid result: "+obj.formatted_content)
    
    def testContentFormattingMarkdown(self):
        obj = Page(title="Format Test", url="/format-test", user=self.user, site=self.site)
        obj.content = "*Markdown* _test_"
        obj.content_format = FORMAT_MARKDOWN
        self.assertEqual(obj.formatted_content, '<p><em>Markdown</em> <em>test</em></p>', "Invalid result: "+obj.formatted_content)
    
    def testAbsoluteURL(self):
        obj = Page(title="URL Test", url="/url-test", user=self.user, site=self.site)
        obj.save()
        obj.site = self.site
        obj.save()
        
        url = obj.get_absolute_url()
        
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, "Could not load page at the URL it claims it's at (%d): %s" % (response.status_code, url))
        
        obj.delete()
    
    def testRevisionHistory(self):
        pass

class RedirectTests(TestCase):
    site = None
    user = None
    
    def setUp(self):
        self.site = Site.objects.get_current()
        self.user = User.objects.create_user(username='testuser', password='12345')
    
    def testPathRedirectTemporary(self):
        r = Redirect(original="/moved-temp", current="/success-temp", permanent=False)
        r.save()
        
        response = self.client.get('/moved-temp')
        self.assertEqual(response.status_code, 302, "Incorrect response code (%s); expected 302." % response.status_code)
        self.assertEqual(response['Location'], "/success-temp", "Unexpected destination: %s" % response['Location'])
        
        r.delete()
    
    def testPathRedirectPermanent(self):
        r = Redirect(original="/moved-perm", current="/success-perm", permanent=True)
        r.save()
        
        response = self.client.get('/moved-perm')
        self.assertEqual(response.status_code, 301, "Incorrect response code (%s); expected 301." % response.status_code)
        self.assertEqual(response['Location'], "/success-perm", "Unexpected destination: %s" % response['Location'])
        
        r.delete()
    
    def testObjectRedirectTemporary(self):
        obj = Page(title="TestTemp", url="/test-temp", content="Lorum ipsum etc.", user=self.user, site=self.site)
        obj.save()
        obj.site = self.site
        obj.save()
        
        r = Redirect(original="/moved-obj-temp", target_object=obj, permanent=False)
        r.save()
        
        response = self.client.get('/moved-obj-temp')
        self.assertRedirects(response, obj.get_absolute_url(), status_code=302)
        
        r.delete()
        obj.delete()
    
    def testObjectRedirectPermanent(self):
        obj = Page(title="TestPerm", url="/test-perm", content="Lorum ipsum etc.", user=self.user, site=self.site)
        obj.save()
        obj.site = self.site
        obj.save()
        
        r = Redirect(original="/moved-obj-perm", target_object=obj, permanent=True)
        r.save()
        
        response = self.client.get('/moved-obj-perm')
        self.assertRedirects(response, obj.get_absolute_url(), status_code=301)
        
        r.delete()
        obj.delete()
    
    def testMissingObjectRedirect(self):
        obj = Page(title="TestGone", url="/test-gone", content="Lorum ipsum etc.", user=self.user, site=self.site)
        obj.save()
        obj.site = self.site
        obj.save()
        
        r = Redirect(original="/missing-obj", target_object=obj)
        r.save()
        
        obj.delete()
        
        response = self.client.get('/missing-obj')
        self.assertEqual(response.status_code, 410, "Incorrect response code (%s); expected 410." % response.status_code)
        try:
            raise Exception("Unexpected destination (should be empty): %s" % response['Location'])
        except KeyError:
            pass
        
        r.delete()
