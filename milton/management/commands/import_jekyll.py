# -*- coding: utf-8 -*-

import datetime
import logging
import os
import re
from optparse import make_option

import yaml
try:
    from yaml import CSafeDumper as SafeDumper
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeDumper
    from yaml import SafeLoader

import dateutil.parser, pytz

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User
from django.db import IntegrityError

from taggit.models import Tag

from milton.models import *
from milton.templatetags.slugify import slugify

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-s','--site-id', action='store', dest='site_id',
            help="Milton Site ID to export records from.")
        parser.add_argument('-u','--site-url', action='store', dest='site_url', 
            help="Lookup an existing Milton site by URL.")
        # parser.add_argument('-b','--blog-section', dest='blog_section', action='store', default="Blog", help="Section name to put blog entries in (empty string for None)."),
        # parser.add_argument('-p','--page-section', dest='page_section', action='store', default="Pages", help="Section name to put pages in (empty string for None)."),
        # parser.add_argument('-g','--global', dest='global', action='store_true', default=False, help="Create items globally instead of site-specific, when possible (currently Sections and Resources)."),
        parser.add_argument('-r', '--replace', dest='replace', action='store_true', default=False, help="Replace matching existing objects instead of skipping them. (DANGER ZONE)"),
        parser.add_argument('dirs', action='append', help = 'a directory of Jekyll source files')
        
    def handle(self, *args, **options):
        show_traceback = options.get('traceback', False)
        
        log = logging.getLogger('milton')
        
        verbosity = int(options.get('verbosity', 0))
        
        if verbosity >=2:
            log.setLevel(logging.DEBUG)
        elif verbosity == 0:
            log.setLevel(logging.WARNING)
        else:
            log.setLevel(logging.INFO)
        
        log.info("Starting import.")
        
        user = User.objects.get(pk=1)
        log.debug("Importing data with %s as the content owner.", user.username)
        
        # Prefetch the destination site
        site_obj = None
        if options.get('site_id', None):
            site_obj = Site.objects.get(pk=options.get('site_id'))
        elif options.get('site_url', None):
            site_obj = Site.objects.get_site_for_URL(options.get('site_url'))
        
        if not site_obj:
            raise CommandError("No site specified. Use either --site-id or --site-url to specify a site for the import.")
        
        # TODO: Categories
        # Always filter in a way that allows site-specific resources as well as global resources.
        # However, create objects assigned to the site unless otherwise specified.
        # site_filter = { 'site__in': [site_obj, None] }
        # if options.get('globally'):
        #     site_arg = None
        # else:
        #     site_arg = site_obj
        
        log.info("Using site %s", site_obj.name)
        
        try:
            for postdir in options['dirs']:
                for path, fm, content in jekyll_site_iter(postdir):
                    log.debug("Importing %s", path)
                    
                    can_replace = (options.get('replace', False) == True)
                    
                    props = {}
                    props.setdefault('slug', 'untitled')
                    
                    # Get the dates.  Only 'date' is proper; the others would come from a previous export.
                    date_published = fm['date']
                    date_created = fm.get('date-created', date_published)
                    date_modified = fm.get('date-modified', date_created)
                    
                    # Add timezones
                    date_published = _fix_date(date_published)
                    date_created = _fix_date(date_created)
                    date_modified = _fix_date(date_modified)
                                        
                    # SPObject
                    #   date_created
                    #   date_modified
                    
                    props['date_created'] = date_created
                    props['date_modified'] = date_modified
                    
                    # Resource
                    #   title
                    #   content_format
                    #   excerpt
                    #   content
                    #   status
                    #   date_published
                    #   date_hidden
                    #   show_metadata
                    #   allow_comments
                    #   user (FK)
                    #   site (FK)
                    
                    props['title'] = fm.get('title', 'Untitled')
                    
                    props.setdefault('content_format', FORMAT_NONE)
                    
                    markup = fm.get('markup', '').lower()
                    if markup == 'textile':
                        props['content_format'] = FORMAT_TEXTILE
                    elif markup == 'markdown':
                        props['content_format'] = FORMAT_MARKDOWN
                    
                    # excerpt will generally only exist from a previous export
                    props['excerpt'] = fm.get('excerpt', '')
                    props['content'] = content
                    
                    if fm.get('published') == False:
                        props['status'] = STATUS_DRAFT
                    props['date_published'] = date_published
                    
                    # date_hidden is not in the source
                    # show_metadata is not in the source
                    # allow_comments is not in the source
                    
                    props['user'] = user
                    props['site'] = site_obj
                    
                    # Story
                    #   slug
                    #   link
                    #   section (FK)
                    #   tags (MM)
                    #   categories (MM)
                                        
                    props['slug'] = fm.get('slug', slugify(props['title']))
                    if len(props['slug']) == 0: del props['slug']
                    
                    # link is not in the source
                    
                    # First Save
                    
                    if can_replace is True:
                        obj, created = Story.objects.update_or_create(
                            date_published=date_published,
                            slug=props['slug'],
                            defaults=props)
                        if created:
                            log.info("Created story [%s,%s]" % (props['slug'], date_published))
                        else:
                            log.info("Updated story [%s,%s]" % (props['slug'], date_published))
                    else:
                        try:
                            obj = Story.objects.create(**props)
                            log.info("Created story [%s,%s]" % (props['slug'], date_published))
                        except IntegrityError:
                            log.warning("Story already exists; skipping [%s,%s]" % (props['slug'], date_published))
                            continue
                    
                    # Second Save - adding relationships after first save
                    
                    tags = fm.get('tags', None)
                    if tags:
                        obj.tags.add(*[t.strip() for t in tags])
                    
                    # TODO: categories (array of strings)
                    
                    # Re-save these properties as some are automatically updated
                    obj.date_created = date_created
                    obj.date_modified = date_modified
                    obj.date_published = date_published
                    obj.save()
                    
                    # log.info("Imported %s (%d)", path, obj.id)
                    
        except Exception as e:
            if show_traceback:
                raise
            raise CommandError("Error: %s" % e)
        
        log.info("Done.")

def jekyll_site_iter(posts_dir):
    if not os.path.isdir(posts_dir): return
    
    drafts_dir = os.path.join(posts_dir, "_drafts")
    posts_dir = os.path.join(posts_dir, "_posts")
    paths = []
    if os.path.exists(drafts_dir):
        paths.append(drafts_dir)
    if os.path.exists(posts_dir):
        paths.append(posts_dir)
    
    for dirpath in paths:
        for root, dirs, files in os.walk(dirpath):
            for filename in files:
                path = os.path.join(root, filename)
                date_str = filename[:10]
                slug, dot, markup = filename[11:].rpartition(".")
                
                # Default publish date
                date = parse_jekyll_date(date_str)
            
                # Default publish status
                if "_drafts" in os.path.split(path):
                    published = False
                else:
                    published = True
                
                # Default markup
                markup = markup.lower()
                if markup == 'md': markup = 'markdown'
                # if markup == 'textile': markup = 'textile' # Implied, but redundant to check for.
                
                f = open(path)
                d = f.read()
                f.close()
                
                fm, content = split_post(d)
                front_matter = yaml.load(fm, Loader=SafeLoader)
                
                meta = {
                    'published': published,
                    'date': date,
                    'slug': slug,
                    'markup': markup,
                }
                for k in meta.keys():
                    front_matter.setdefault(k, meta[k])
                            
                content = content.replace("<!--break-->", "\n\n")  # Not Jekyll, but from previous migrations.
            
                yield (filename, front_matter, content)


FM_BOUNDARY = re.compile(r'^-{3,}$', re.MULTILINE)
def split_post(text):
    global FM_BOUNDARY
    _, fm, content = FM_BOUNDARY.split(text, 2)
    return (fm, content)

def parse_jekyll_date(s):
    d = None
    try:
        # 1002345678
        d = datetime.datetime.fromtimestamp(float(s.strip())) #throws ValueError
    except ValueError:
        # 2000-01-01
        # d = datetime.datetime.strptime(s.strip(), '%Y-%m-%d')
        d = dateutil.parser.parse(s.strip())
    d = _fix_date(d)
    return d

def _fix_date(d):
    if not d: return None
    if d.tzinfo == None:
        d = pytz.utc.localize(d)
    else:
        d = d.replace(tzinfo=pytz.utc)
    return d
