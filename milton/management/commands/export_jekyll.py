# -*- coding: utf-8 -*-

from io import StringIO
import datetime
import logging
import os, sys
import yaml
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User

from milton.models import *


class Command(BaseCommand):
    help = 'Export a directory of Jekyll-compatible source files.'
    
    def add_arguments(self, parser):
        # parser.add_argument('-v', '--verbosity', dest='verbosity', action="store", default="0", type="choice", choices=['0','1','2'], help='Verbose output'),
        parser.add_argument('-s','--site-id', action="store", dest='site_id',
            help="Milton Site ID to export records from.")
        parser.add_argument('-u','--site-url', action="store", dest='site_url', 
            help="Lookup an existing Milton site by URL.")
        parser.add_argument('-r', '--replace', action="store_true", dest='replace',
            help="Replace existing files instead of skipping them. (DANGER ZONE)")
        parser.add_argument('dir')
    
    def handle(self, *args, **options):
        show_traceback = options.get('traceback', False)
        
        log = logging.getLogger("milton")
        
        verbosity = int(options.get('verbosity', 0))
        
        if verbosity >=2:
            log.setLevel(logging.DEBUG)
        elif verbosity == 0:
            log.setLevel(logging.WARNING)
        else:
            log.setLevel(logging.INFO)
        
        dstpath = options.get('dir', None)
        if not dstpath:
            raise CommandError("You must set a destination for the export directory.")
        
        # Get the source site
        site_obj = None
        if options.get("site_id", None):
            site_obj = Site.objects.get(pk=options.get("site_id"))
        elif options.get("site_url", None):
            site_obj = Site.objects.get_site_for_URL(options.get("site_url"))
        
        if not site_obj:
            raise CommandError("No site specified. Use either --site-id or --site-url to specify a site to export.")
        
        log.info("Exporting site %s", site_obj.name)
        
        # YAML output settings.
        yaml_opts = {
            "allow_unicode": True,
            "canonical": False,
            "default_flow_style": False,
            "default_style": "",
            "encoding": "utf-8",
            "explicit_start": True,
            "indent": 2,
        }
        
        try:
            drafts_path = os.path.join(dstpath, "_drafts")
            posts_path = os.path.join(dstpath, "_posts")
            
            # Verify destination is not a file, if it exists.
            if not os.path.isdir(dstpath):
                if os.path.exists(dstpath):
                    raise CommandError("%s exists, but isn't a directory." % dstpath)
            
            # Create destination dirs, if needed.
            os.makedirs(dstpath, exist_ok=True)
            os.makedirs(drafts_path, exist_ok=True)
            os.makedirs(posts_path, exist_ok=True)
            
            # Save Stories into _drafts, _posts
            for story in Story.objects.filter(site=site_obj):
                # Start with a base path depending on published status
                # Note that a choice has been made to not use the
                #   hidden date brackets here and use the more semantic
                #   resource status flag.
                if story.status == STATUS_PUBLISHED:
                    export_path = posts_path
                else:
                    export_path = drafts_path
                
                # Build the filename
                date = story.date_published or story.date_created
                export_path = os.path.join(export_path, "%04d-%02d-%02d-%s" % (
                    date.year,
                    date.month,
                    date.day,
                    story.slug,
                ))
                
                # Determine the file extension
                story_format = None
                if story.content_format == FORMAT_MARKDOWN:
                    export_path += ".md"
                    story_format = "markdown"
                elif story.content_format == FORMAT_TEXTILE:
                    export_path += ".textile"
                    story_format = "textile"
                else:
                    export_path += ".txt"
                
                # Check for existing files
                filename = os.path.basename(export_path)
                if os.path.exists(export_path):
                    if not options.get("replace", False):
                        log.info("file exists: %s; skipping.", filename)
                        continue
                    else:
                        log.info("replacing %s", filename)
                else:
                    log.info("creating %s", filename)
                
                # Build the YAML front matter header
                props = {
                    "layout": "blog",
                    "permalink": story.get_absolute_url(),
                    "title": story.title,
                    "slug": story.slug,
                    "user": story.user.username,
                    "date": story.date_published,
                    "date-created": story.date_created,
                    "date-modified": story.date_modified,
                }
                if story.section:
                    props["section"] = story.section.slug
                if story.link and len(story.link):
                    props["link"] = story.link
                if story.categories.all().count():
                    props["categories"] = [o.name for o in story.categories.all()]
                if story.tags.all().count():
                    props["tags"] = [o.name for o in story.tags.all()]
                if story.excerpt and len(story.excerpt):
                    props["excerpt"] = story.excerpt
                if story_format:
                    props["markup"] = story_format
                
                # Write out the header and content to a file.
                with open(export_path, "w") as f:
                    yaml.safe_dump(props, f, **yaml_opts)
                    f.write("---\n")
                    f.write(story.content + "\n")
                
            # Save Pages into base dir
            
            
        except Exception as e:
            if show_traceback:
                raise
            raise CommandError("Error: %s" % e)
        
        log.info("Done.")
