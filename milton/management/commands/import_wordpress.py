# -*- coding: utf-8 -*-

import datetime
import logging
import xml.etree.cElementTree as ET
from optparse import make_option

import dateutil.parser, pytz

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.auth.models import User

from taggit.models import Tag

from milton.models import Section, Category, Page, Story, Redirect, ContentHistory, Site
from milton.templatetags.slugify import slugify

def _isodate_to_date(s):
    if not s: return None
    d = dateutil.parser.parse(s)
    if d.tzinfo == None:
        d = pytz.utc.localize(d)
    else:
        d = d.replace(tzinfo=pytz.utc)
    return d

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('-s', '--site-id', dest='site_id', action="store", help="Milton Site ID to import records to."),
        make_option('-u', '--site-url', dest='site_url', action="store", help="Lookup an existing Milton site by URL."),
        make_option('-r', '--replace', dest='replace', action="store_true", default=False, help="Replace matching existing objects instead of skipping them. (DANGER ZONE)"),
    )
    help = 'Import a Wordpress export file.'
    args = 'file.xml'
    
    def handle(self, *files, **options):
        log = logging.getLogger("milton")
        
        opt_verbose = int(options.get('verbosity', 0))
        opt_show_traceback = options.get('traceback', False)
        opt_replace = options.get('replace', False)
        debug = (opt_verbose == 2)
        
        user = User.objects.get(pk=1)
        
        if debug:
            log.setLevel(logging.DEBUG)
        elif opt_verbose:
            log.setLevel(logging.INFO)
        else:
            log.setLevel(logging.WARNING)
        
        log.info("Starting import.")
        log.debug("Importing data with %s as the content owner.", user.username)
        
        # Prefetch the destination site
        site_obj = None
        if options.get("site_id", None):
            site_obj = Site.objects.get(pk=options.get("site_id"))
        elif options.get("site_url", None):
            site_obj = Site.objects.get_site_for_URL(options.get("site_url"))
        
        if not site_obj:
            raise CommandError("No site specified. Use either --site-id or --site-url to specify a site for the import.")
        
        # Always filter in a way that allows site-specific resources as well as global resources.
        # However, create objects assigned to the site unless otherwise specified.
        site_filter = { 'site__in': [site_obj, None] }
        if options.get("globally"):
            site_arg = None
        else:
            site_arg = site_obj

        log.info("Using site %s", site_obj.name)
        
        namespaces = {
            "content": "http://purl.org/rss/1.0/modules/content/",
            "dc": "http://purl.org/dc/elements/1.1/",
            "excerpt": "http://wordpress.org/export/1.2/excerpt/",
            "wfw": "http://wellformedweb.org/CommentAPI/",
            "wp": "http://wordpress.org/export/1.2/",
        }
        
        try:
            for path in files:
                tree = ET.parse(path)
                root = tree.getroot()
                if root.tag != "rss":
                    raise Exception("Invalid export file: root tag is not 'rss'")
                
                for channel in root.findall("channel", namespaces):
                    elm_title = channel.find("title", namespaces)
                    elm_link = channel.find("link", namespaces)
                    elm_description = channel.find("description", namespaces)
                    log.info("%s: %s (%s)" % (elm_title.text, elm_description.text, elm_link.text))
                    
                    site_obj.base_url = elm_link.text
                    site_obj.name = elm_title.text
                    site_obj.slogan = elm_description.text
                    site_obj.save()
                    
                    elm_items = channel.findall("item", namespaces)
                    elm_tags = channel.findall('wp:tag', namespaces)
                    elm_categories = channel.findall('wp:category', namespaces)
                    log.info("%d items, %d categories, %d tags" % (len(elm_items), len(elm_categories), len(elm_tags)))
                    
                    for tag in elm_tags:
                        name = tag.find("wp:tag_name", namespaces).text
                        slug = tag.find("wp:tag_slug", namespaces).text
                        
                        if Tag.objects.filter(slug=slug).count() == 0:
                            t = Tag(name=name, slug=slug)
                            log.info("Tag: %s" % t.name)
                            t.save()
                        
                    for category in elm_categories:
                        name = category.find("wp:cat_name", namespaces).text
                        slug = category.find("wp:category_nicename", namespaces).text
                        if Category.objects.filter(slug=slug).count() == 0:
                            c = Category(name=name, slug=slug, site=site_obj)
                            log.info("Category: %s" % c.name)
                            c.save()
                    
                    for item in elm_items:
                        t = item.find("wp:post_type", namespaces).text
                        
                        # Post/Blog/Entry/Story
                        if t == "post":
                            #TODO: Get essential attrs (date/slug) and test for existance first.
                            pubDate = item.find("pubDate")
                            pubDate = _isodate_to_date(pubDate.text)
                            slug = item.find("wp:post_name", namespaces).text
                            
                            q = Story.objects.filter(date_published = pubDate, slug = slug)
                            if q.count() != 0:
                                if opt_replace == False:
                                    log.info("Story already exists; skipping [%s,%s]" % (slug, pubDate))
                                    continue
                                else:
                                    log.info("Story already exists; replacing [%s,%s]" % (slug, pubDate))
                                    obj = q[0]
                            else:
                                obj = Story(site=site_obj)
                            
                            for element in item.iter():
                                tag = element.tag
                                text = element.text
                                if text and len(text):
                                    if tag == "title":
                                        obj.title = text
                                    elif tag == "link":
                                        obj.link = text
                                    elif tag == "{%s}creator" % namespaces['dc']:
                                        u = User.objects.get(username=text)
                                        if u:
                                            obj.user = u
                                    elif tag == "pubDate":
                                        obj.date_published = _isodate_to_date(text)
                                    elif tag == "guid":
                                        #TODO: Create Redirect for this URL, if it's a URL
                                        pass
                                    elif tag == "{%s}encoded" % namespaces['content']:
                                        obj.content = text
                                    elif tag == "description":
                                        obj.content = text
                                    elif tag == "{%s}encoded" % namespaces['excerpt']:
                                        obj.excerpt = text
                                    elif tag == "{%s}post_date" % namespaces['wp']:
                                        obj.date_created = _isodate_to_date(text)
                                        obj.date_modified = _isodate_to_date(text)
                                    elif tag == "{%s}post_name" % namespaces['wp']:
                                        obj.slug = text
                                    
                            if not obj.slug:
                                obj.slug = slugify(obj.title)
                            
                            log.info("Story: %s - %s", obj.date_published, obj.title)
                            obj.save()
                            
                            for element in item.iterfind("category"):
                                kind = element.get('domain')
                                if kind == "category":
                                    c = Category.objects.get(slug=element.get('nicename'))
                                    obj.categories.add(c)
                                elif kind == "post_tag":
                                    obj.tags.add(element.get('nicename'))
        
        except Exception as e:
            if opt_show_traceback:
                raise
            raise CommandError("Error: %s" % e)
        
        log.info("Done.")
