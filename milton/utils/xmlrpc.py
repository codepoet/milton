# -*- coding: utf-8 -*-

# Based on http://www.allyourpixel.com/post/xmlrpc-djgo/ by Greg Abbas, but mostly gutted.
# That was based on Amit Upadhyay's xmlrpc implementation, which is no longer available:
#   http://nerdierthanthou.nfshost.com/2005/09/xmlrpc-support-for-django.html

import sys
import traceback
import xmlrpc.server

from django.http import HttpResponse

from .errors import XMLRPCError

def view(request, module):
    dispatcher = xmlrpc.server.SimpleXMLRPCDispatcher(True, "utf-8")
    
    try:
        mod = __import__(module, '', '', [''])
    except ImportError as e:
        raise EnvironmentError("Could not import module '%s' (is it on sys.path?): %s" % (module, e))

    dispatcher.register_instance(mod)
    dispatcher.register_introspection_functions()

    try:
        if request.META['REQUEST_METHOD'] != 'POST':
            raise XMLRPCMethodNotAllowedError()
        
        # get arguments
        data = request.body
        response = dispatcher._marshaled_dispatch(
                data, getattr(mod, '_dispatch', None)
            )
    
    except XMLRPCError as e:
        response = xmlrpc.server.xmlrpclib.dumps(
            xmlrpc.server.xmlrpclib.Fault(e.code, e.message)
        )
        
    except Exception as e:
        # traceback.print_last()
        # internal error, report as HTTP server error
        response = xmlrpc.server.xmlrpclib.dumps(
            xmlrpc.server.xmlrpclib.Fault(1, "%s:%s" % (
                    sys.exc_info()[0], sys.exc_info()[1]))
        )
    return HttpResponse(response, content_type="text/xml")
