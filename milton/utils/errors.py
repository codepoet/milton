class XMLRPCError(Exception):
	message = "Internal server error"
	code = 500
	
	def init(message=None, code=None):
		if message:
			self.message = message
		if code:
			self.code = code

class XMLRPCBadRequestError(Exception):
	message = "Bad Request"
	code = 400

class XMLRPCUnauthorizedError(Exception):
	message = "Unauthorized"
	code = 401

class XMLRPCForbiddenError(Exception):
	message = "Forbidden"
	code = 403

class XMLRPCNotFoundError(Exception):
	message = "Not Found"
	code = 404

class XMLRPCMethodNotAllowedError(Exception):
	message = "Method Not Allowed"
	code = 405

class XMLRPCConflictError(Exception):
	message = "Conflict"
	code = 409

class XMLRPCGoneError(Exception):
	message = "Gone"
	code = 410

