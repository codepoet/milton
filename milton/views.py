# -*- coding: utf-8 -*-

from logging import getLogger
logger = getLogger(__name__)

from django.conf import settings
from django.contrib.syndication.views import Feed
from django.core.exceptions import ImproperlyConfigured
from django.urls import reverse
from django.http import Http404
from django.views.generic import ListView, DetailView

from milton.models import *


### Stories ###

class StoryDetail(DetailView):
    # SingleObjectTemplateResponseMixin
    template_name = "milton/story_detail.html"
    
    # SingleObjectMixin
    model = Story
    queryset = Story.published
    context_object_name = "object"
    
    # View
    http_method_names = ['get', 'head', 'options', 'trace']
    
    def get_context_data(self, *args, **kwargs):
        # Call the base implementation first to get a context
        context = super(StoryDetail, self).get_context_data(**kwargs)
        context['title'] = kwargs['object'].title
        return context
    
    def get_object(self, queryset=None):
        section = self.kwargs['section']
        year = int(self.kwargs['year'])
        month = int(self.kwargs['month'])
        day = int(self.kwargs['day'])
        slug = self.kwargs['slug']
        
        try:
            if section is None:
                story = Story.published.get(
                	section = None,
                    date_published__year = year,
                    date_published__month = month,
                    date_published__day = day,
                    slug = slug
                )
            else:
                story = Story.published.get(
                    section__slug = section,
                    date_published__year = year,
                    date_published__month = month,
                    date_published__day = day,
                    slug = slug
                )
        except Story.DoesNotExist as e:
            # No result?  404.
            raise Http404
        except Story.MultipleObjectsReturned as e:
            # If you don't have a unique section-date-slug combo for your section (or lack thereof) then this triggers.
            raise ImproperlyConfigured("Multiple objects exist for the specified section, date, and name.")
        return story


class StoryList(ListView):
    # TemplateResponseMixin
    template_name = "milton/story_list.html"
    
    # MultipleObjectMixin
    model = Story
    paginate_by = settings.MILTON_PAGE_SIZE
    
    # View
    http_method_names = ['get', 'head', 'options', 'trace']
    
    def get_queryset(self):
        qs = Story.published.all()
        if self.kwargs['section'] is not None:
            qs = qs.filter(section__slug=self.kwargs['section'])
        if self.kwargs['year'] is not None:
            qs = qs.filter(date_published__year=int(self.kwargs['year']))
        if self.kwargs['month'] is not None:
            qs = qs.filter(date_published__month=int(self.kwargs['month']))
        if self.kwargs['day'] is not None:
            qs = qs.filter(date_published__day=int(self.kwargs['day']))
        return qs
    
    def get_context_data(self, *args, **kwargs):
        # Call the base implementation first to get a context
        context = super(StoryList, self).get_context_data(**kwargs)

        context.update({
            "options": {
                "excerpts": settings.MILTON_ARCHIVES_SHOW_EXCERPT_ONLY
            },
        })

        year = self.kwargs['year'] is not None
        month = self.kwargs['month'] is not None
        day = self.kwargs["day"] is not None

        if year and month and day:
            context['title'] = "%04d-%02d-%02d" % (int(self.kwargs['year']), int(self.kwargs['month']), int(self.kwargs['day']))
        elif year and month:
            context['title'] = "%04d-%02d" % (int(self.kwargs['year']), int(self.kwargs['month']))
        elif year:
            context['title'] = "%04d" % int(self.kwargs['year'])
        
        if 'title' in context:
            context['title'] = "Stories from " + context['title']
        elif self.kwargs['section'] is not None:
            context['title'] = self.kwargs['section']
        else:
            context['title'] = ""
        
        return context

class PageDetail(DetailView):
    # SingleObjectTemplateResponseMixin
    template_name_field = "template_name"
    
    # SingleObjectMixin
    model = Page
    queryset = Page.published
    context_object_name = "object"
    
    # View
    http_method_names = ['get', 'head', 'options', 'trace']
    
    def get_object(self):
        url = self.args[0]
        
        # URLs begin with a slash, but a bad admin or urls.py can remove it.
        if not url.startswith('/'):
            url = "/" + url
        
        try:
            page = Page.published.get(url__exact=url)
        
        except Page.DoesNotExist as e:
            raise Http404
        
        except Page.MultipleObjectsReturned as e:
            #You broke it, man.  One URL = One Object.
            raise ImproperlyConfigured("Multiple objects exist for the specified URL: %s." % url)
        
        return page
    
    def get_context_data(self, *args, **kwargs):
        # Call the base implementation first to get a context
        context = super(PageDetail, self).get_context_data(**kwargs)
        page = kwargs['object']
        context.update({
            "title":  page.title,
            "flatpage": page, #Let people use the same template to a degree
        })
        return context
    
    def get_template_names(self):
        templates = ["milton/page_detail.html", "milton/page.html"]
        
        # Look for the template name specified on the Page object.
        if self.object is not None:
            t = getattr(self.object, self.template_name_field)
            if t is not None and len(t):
                templates.insert(0, getattr(self.object, self.template_name_field))
        
        return templates

class StoryListByTag(ListView):
    model = Story
    paginate_by = settings.MILTON_PAGE_SIZE
    template_name = "milton/story_list.html"
    
    def get_queryset(self):
        tag_name = self.kwargs['tag_slug']
        return Story.published.filter(tags__slug=tag_name)
    
    def get_context_data(self, *args, **kwargs):
        # Call the base implementation first to get a context
        context = super(StoryListByTag, self).get_context_data(**kwargs)
        
        context.update({
            "options": { "excerpts": settings.MILTON_ARCHIVES_SHOW_EXCERPT_ONLY },
            "title": "Stories tagged %s" % self.kwargs['tag_slug'],
        })
        
        return context

view_tag = StoryListByTag.as_view()


### FEEDS ###

class MainSiteFeed(Feed):
    def title(self):
        site = Site.objects.get_current()
        return site.name
    
    def description(self):
        site = Site.objects.get_current()
        return site.slogan
    
    def link(self):
        return reverse('site-feed')
    
    def items(self):
        return Story.objects.all()[:15]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.formatted_content
        
    def item_pubdate(self, item):
        return item.date_published
    
    def item_updateddate(self, item):
        return item.date_modified
