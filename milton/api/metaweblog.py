# -*- coding: utf-8 -*-

# http://www.allyourpixel.com/post/metaweblog-38-django/

from functools import wraps
import datetime
import dateutil.parser
import urllib.parse
import xmlrpc.client # import DateTime

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.files.base import ContentFile

from taggit.models import Tag

from milton.models import *
from milton.templatetags.slugify import slugify
from milton.utils.errors import *


'''
Each function that will be made available over XML-RPC is wrapped with @register.  That adds it to a global that the _listMethods and _dispatch methods use to service requests from SimpleXMLRPCServer when this module is assigned as an instance/datasource.  Pretty straightforward.
'''

_rpclookup = {}

def register(method, public=False):
    def _wrapper(f):
        @wraps(f)
        def _inner(*args, **kwargs):
            return f(*args, **kwargs)
        global _rpclookup
        f.public = public
        _rpclookup[method] = f
        return _inner
    return _wrapper

def _listMethods():
    global _rpclookup
    return [m for m in list(_rpclookup.keys()) if _rpclookup[m].public]
    
def _dispatch(method, params):
    global _rpclookup
    try:
        func = _rpclookup[method]
        return func(*params)
    except KeyError as e:
        raise Exception("unknown method: %s" % method)


def authenticated(pos=1):
    """
    A decorator for functions that require authentication.
    Assumes that the username & password are the second & third parameters.
    Doesn't perform real authorization (yet), it just checks that the
    user is_superuser.
    """
    
    def _decorate(func):
        def _wrapper(*args, **kwargs):
            username = args[pos+0]
            password = args[pos+1]
            args = args[0:pos]+args[pos+2:]
            try:
                user = User.objects.get(username__exact=username)
            except User.DoesNotExist:
                raise XMLRPCUnauthorizedError()
            if not user.check_password(password):
                raise XMLRPCUnauthorizedError()
            if not user.is_superuser:
                raise XMLRPCForbiddenError()
            return func(user, *args, **kwargs)
        
        return _wrapper
    return _decorate

def full_url(url=''):
    return urllib.parse.urljoin(Site.objects.get_current().base_url, url)

# example... this is what wordpress returns:
# {'permaLink': 'http://gabbas.wordpress.com/2006/05/09/hello-world/',
#  'description': 'Welcome to <a href="http://wordpress.com/">Wordpress.com</a>. This is your first post. Edit or delete it and start blogging!',
#  'title': 'Hello world!',
#  'mt_excerpt': '',
#  'userid': '217209',
#  'dateCreated': <DateTime u'20060509T16:24:39' at 2c7580>,
#  'link': 'http://gabbas.wordpress.com/2006/05/09/hello-world/',
#  'mt_text_more': '',
#  'mt_allow_comments': 1,
#  'postid': '1',
#  'categories': ['Uncategorized'],
#  'mt_allow_pings': 1}

def _date_to_isodate(d):
    if not d: return None
    return xmlrpc.client.DateTime(d.isoformat())

def _isodate_to_date(s):
    if not s: return None
    return dateutil.parser.parse(s)
    

def _make_struct_from_story(story):
    link = full_url(story.get_absolute_url())
    post_status = 'new'
    if story.visible:
        post_status = 'publish'
    elif story.status == STATUS_PUBLISHED:
        post_status = 'future'
    elif story.status == STATUS_DRAFT:
        post_status = 'draft'
    
    struct = {
        # MetaWeblog
        'categories': [_make_struct_from_category(c) for c in story.categories.all()],
        'dateCreated': _date_to_isodate(story.date_created),
        'description': story.content,
        'link': link,
        'permaLink': link,
        'postid': story.id,
        'title': story.title,
        'userid': story.user.id,

        # Movable Type
        # 'mt_text_more': story.content,
        'mt_excerpt': story.excerpt,
        'mt_keywords': ",".join(story.tags.names()),
        'mt_tags': ",".join(story.tags.names()),
        'mt_basename': story.slug,
        'mt_convert_breaks': str(story.content_format), #Sadly, we have to use one format for both components.
        'mt_allow_comments': story.allow_comments,
        'mt_allow_pings': 0, # 1 or 0

        # 'mt_tb_ping_urls': [], # Only on create/edit.

        # Wordpress
        'date_modified': _date_to_isodate(story.date_modified),
        # 'date_created_gmt': _date_to_isodate(story.date_created),
        # 'date_modified_gmt': _date_to_isodate(story.date_modified),
        # 'wp_more_text': story.content,
        'wp_slug': story.slug,
        'post_status': post_status,
        }
    if story.date_published:
        struct['dateCreated'] = _date_to_isodate(story.date_published)
    
    return struct

def _make_struct_from_page(page):
    link = full_url(page.get_absolute_url())
    post_status = 'new'
    if page.visible:
        post_status = 'publish'
    elif page.status == STATUS_PUBLISHED:
        post_status = 'future'
    elif page.status == STATUS_DRAFT:
        post_status = 'draft'
        
    struct = {
        # MetaWeblog
        'dateCreated': _date_to_isodate(page.date_created),
        'description': page.content,
        'link': link,
        'permaLink': link,
        'page_id': page.id,
        'title': page.title,

        # Movable Type
        'mt_excerpt': page.excerpt,
        'mt_basename': page.slug,
        'mt_convert_breaks': str(page.content_format),

        # Wordpress
        'date_modified': _date_to_isodate(page.date_modified),
        'wp_slug': page.slug,
        'post_status': post_status,
        'wp_page_template': post.template_name,
        }
    if page.date_published:
        struct['dateCreated'] = _date_to_isodate(page.date_published)
    
    return struct

def _make_struct_from_section(section):
    if section == None: return None
    struct = {
        'categoryId': section.id,
        'categoryName': section.name,
        'isPrimary': False,
    }
    return struct

def _make_struct_from_category(category):
    if category == None: return None
    struct = {
        'categoryId': category.id,
        'categoryName': category.name,
        'isPrimary': False,
    }
    return struct

def _make_tag_struct(tag):
    if tag == None: return None
    return {
        "tag_id": tag.id,
        "name": tag.name,
        "slug": tag.slug,
    }

def _update_post(post, struct):
    # TODO: Add remaining properties
    if struct.get('dateCreated', None):
        post.date_published = _isodate_to_date(struct['dateCreated'].value)
    if 'title' in struct:
        post.title = struct['title']
    if 'description' in struct:
        post.content = struct['description']
    # if struct.has_key('mt_text_more'):
    #     post.content = struct['mt_text_more']
    if 'mt_basename' in struct:
        post.slug = struct['mt_basename']
    if 'wp_slug' in struct:
        post.slug = struct['wp_slug']
    if 'mt_convert_breaks' in struct:
        post.content_format = struct['mt_convert_breaks']
    if 'post_status' in struct:
        status = struct['post_status']
        # Choices: new, publish, future, draft
        if status == 'draft':
            post.status = STATUS_DRAFT
        else:
            post.status = STATUS_PUBLISHED
    if not post.slug or len(post.slug) == 0:
        post.slug = slugify(post.title)

def _update_post_tags(post, struct):
    if 'mt_tags' in struct:
        post.tags.set(*(struct['mt_tags'].split(",")))
    elif 'mt_keywords' in struct:
        post.tags.set(*(struct['mt_keywords'].split(",")))


# -- blogger v1

@register(method="blogger.newPost", public=True)
@authenticated(pos=2)
def blogger_newPost(user, appkey, blogid, content, publish):
    post = Story(status=publish)
    post.user = user
    post.content = content
    post.slug = datetime.datetime.now().time().isoformat # Well, that should be unique, if ugly.
    post.site = Site.objects.get(id=blogid)
    post.save()
    return post.id

@register(method="blogger.editPost", public=True)
@authenticated(pos=2)
def blogger_editPost(user, appkey, postid, content, publish):
    try:
        post = Story.objects.get(id=postid, user=user)
        post.content = content
        post.status = publish
        post.save()
        return post.id
    except Story.DoesNotExist:
        raise Exception("no such story")

@register(method="blogger.deletePost", public=True)
@authenticated(pos=2)
def blogger_deletePost(user, appkey, postid, publish):
    try:
        post = Story.objects.get(id=postid, user=user)
        post.delete()
        return True
    except Story.DoesNotExist:
        raise Exception("no such story")

@register(method="blogger.getUsersBlogs", public=True)
@authenticated(pos=1)
def blogger_getUsersBlogs(user, appkey):
    """
    an array of <struct>'s containing the ID (blogid), name
    (blogName), and URL (url) of each blog.
    """
    sites = Site.objects.all()
    result = [{
                'blogid': str(site.pk),
                'blogName': site.name,
                'url': site.base_url
            } for site in sites]
    return result

@register(method="blogger.getUserInfo", public=True)
@authenticated()
def blogger_getUserInfo(user, appkey):
    return {
        'nickname':user.username,
        'userid':user.id,
        'url':full_url(),
        'email':user.email,
        'lastname':user.last_name,
        'firstname':user.first_name,
        }



# -- metaWeblog

@register(method="metaWeblog.getPost", public=True)
@authenticated()
def metaWeblog_getPost(user, postid):
    try:
        post = Story.objects.get(id=postid, user=user)
        return _make_struct_from_story(post)
    except Story.DoesNotExist:
        raise Exception("no such story")

@register(method="metaWeblog.getRecentPosts", public=True)
@authenticated()
def metaWeblog_getRecentPosts(user, blogid, num_posts):
    if num_posts > 250: num_posts = 250
    posts = Story.objects.filter(user=user, site__id=blogid).order_by('-date_published')[:int(num_posts)]
    return [_make_struct_from_story(post) for post in posts]

@register(method="metaWeblog.newPost", public=True)
@authenticated()
def metaWeblog_newPost(user, blogid, struct, publish):
    post = Story(status = publish)
    post.user = user
    _update_post(post, struct)
    site = Site.objects.get(id__exact=blogid)
    post.site = site
    post.save()
    
    _update_post_tags(post, struct)
    post.save()
    
    return post.id

@register(method="metaWeblog.editPost", public=True)
@authenticated()
def metaWeblog_editPost(user, postid, struct, publish):
    try:
        post = Story.objects.get(id=postid, user=user)
        post.status = publish
        _update_post(post, struct)
        _update_post_tags(post, struct)
        post.save()
        return True
    except Story.DoesNotExist:
        raise Exception("no such story")

@register(method="metaWeblog.deletePost", public=True)
@authenticated(pos=2)
def metaWeblog_deletePost(user, appkey, postid, publish):
    try:
        post = Story.objects.get(id=postid, user=user)
        post.delete()
        return True
    except Story.DoesNotExist:
        raise Exception("no such story")

@register(method="metaWeblog.getCategories", public=True)
@authenticated()
def metaWeblog_getCategories(user, blogid):
    categories = Category.objects.filter( Q(site__id=blogid) | Q(site=None) )
    result = [_make_struct_from_category(category) for category in categories]
    return result

# http://qoli.de/blog/2007/nov/19/implementing-metaweblog-api/
@register(method="metaWeblog.newMediaObject", public=True)
@authenticated()
def metaWeblog_newMediaObject(user, blogid, struct):
    bits = struct['bits'].data
    name = struct['name']
    mime = struct['type']

    media_object = MediaObject(filename=name, content_type=mime, user=user)
    media_object.content.save(name, ContentFile(bits))
    media_object.save()

    return {'url': media_object.content.url}

# -- MovableType

@register(method="mt.getPostCategories", public=True)
@authenticated()
def mt_getPostCategories(user, postid):
    try:
        post = Story.objects.get(id=postid, user=user)
        categories = [_make_struct_from_category(c) for c in post.categories.all()]
        return categories
    except Story.DoesNotExist:
        raise Exception("no such story")

@register(method="mt.setPostCategories", public=True)
@authenticated()
def mt_setPostCategories(user, postid, struct):
    post = Story.objects.get(id=postid, user=user)
    
    categories = []
    for category in struct:
        c = Category.objects.get(id=category['categoryId'])
        categories.append(c)
    
    post.categories = categories
    post.save()
    
    return True

@register(method="mt.getCategoryList", public=True)
@authenticated()
def mt_getCategoryList(user, blogid):
    return [_make_struct_from_category(c) for c in Category.objects.filter( Q(site__id=blogid) | Q(site=None) )]

@register(method="mt.supportedTextFilters", public=True)
def mt_supportedTextFilters():
    return [{'label': f[1], 'key': str(f[0])} for f in CONTENT_FORMATTERS]

# -- WordPress

@register(method="wp.getTags", public=True)
@authenticated()
def wp_getTags(user, blogId):
    return [_make_tag_struct(tag) for tag in Tag.objects.all()]

@register(method="wp.getCategories", public=True)
@authenticated()
def wp_getCategories(user, blogId):
    return [_make_struct_from_category(c) for c in Category.objects.filter( Q(site__id=blogId) | Q(site=None) )]

@register(method="wp.newCategory", public=True)
@authenticated()
def wp_newCategory(user, blogId, category_struct):
    # name, slug, parent_id, description
    category = Category()
    category.name = category_struct['name']
    if 'slug' in category_struct:
        category.slug = category_struct['slug']
    else:
        category.slug = slugify(category.name)
    category.site = blogId
    category.save()
    return category.id

@register(method="wp.getPages", public=True)
@authenticated()
def wp_getPages(user, blogId, count=10):
    pages = Page.objects.filter( Q(site__id=blogId) | Q(site=None) )[:count]
    result = [_make_struct_from_page(p) for p in pages]
    return result

@register(method="wp.getPage", public=True)
@authenticated()
def wp_getPage(user, blogId, pageId):
    try:
        pages = Page.objects.get(id=pageId)
        result = [_make_struct_from_page(p) for p in pages]
        return result
    except Page.DoesNotExist:
        raise Exception("no such page")

