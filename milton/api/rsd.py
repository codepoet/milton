# -*- coding: utf-8 -*-

'''
<rsd xmlns="http://archipelago.phrasewise.com/rsd" version="1.0">
    <service>
        <engineName>Milton</engineName>
        <engineLink>http://bitbucket.org/codepoet/milton/</engineLink>
        <homePageLink>http://example.com</homePageLink>
        <apis>
            <api name="WordPress" blogID="1" preferred="true" apiLink="http://example.com/api/xmlrpc/"/>
            <api name="Movable Type" blogID="1" preferred="false" apiLink="http://example.com/api/xmlrpc/"/>
            <api name="MetaWeblog" blogID="1" preferred="false" apiLink="http://example.com/api/xmlrpc/"/>
            <api name="Blogger" blogID="1" preferred="false" apiLink="http://example.com/api/xmlrpc/"/>
        </apis>
    </service>
</rsd>
'''

from logging import getLogger
from lxml import etree

from django.conf import settings
from django.urls import reverse
from django.http import HttpResponse
from django.views.decorators.cache import cache_control

from milton.models import *


logger = getLogger(__name__)

@cache_control(max_age=60)
def rsd_view(request):
    site = Site.objects.get_current()
    site_url = request.scheme + "://" + request.get_host()
    site_id = str(site.id)
    endpoint = site_url + reverse('api-xmlrpc')

    rsd = etree.Element('rsd', xmlns="http://archipelago.phrasewise.com/rsd", version="1.0")
    service = etree.Element('service')
    rsd.append(service)

    element = etree.Element('engineName')
    element.text = "Milton"
    service.append(element)

    element = etree.Element('engineLink')
    element.text = "http://bitbucket.org/codepoet/milton/"
    service.append(element)

    element = etree.Element('homePageLink')
    element.text = site_url
    service.append(element)

    apis = etree.Element("apis")
    service.append(apis)

    apis.append(etree.Element("api", name="WordPress", blogID=site_id, preferred="true", apiLink=endpoint))
    apis.append(etree.Element("api", name="Movable Type", blogID=site_id, preferred="false", apiLink=endpoint))
    apis.append(etree.Element("api", name="MetaWeblog", blogID=site_id, preferred="false", apiLink=endpoint))
    apis.append(etree.Element("api", name="Blogger", blogID=site_id, preferred="false", apiLink=endpoint))

    s = etree.tostring(rsd, pretty_print=True)
    
    return HttpResponse(s, content_type="application/rsd+xml")
    