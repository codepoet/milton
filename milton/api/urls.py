# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from django.views.decorators.csrf import csrf_exempt

from milton.utils.xmlrpc import view as xmlrpc_view
from milton.api.rsd import rsd_view

urlpatterns = [
    url(
        regex  = r'^xmlrpc$',
        view   = csrf_exempt(xmlrpc_view),
        kwargs = { 'module': 'milton.api.metaweblog' },
        name   = "api-xmlrpc",
    ),
    url(
        regex  = r'^rsd.xml$',
        view   = rsd_view,
        name   = "api-rsd",
    )
]
