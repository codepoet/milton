# -*- coding: utf-8 -*-

from django.db import connections

class DrupalRouter(object):
    """
    A router to use the "drupal" database for Drupal objects, if it exists.
    """
    app_name = 'drupal_support'
    database_name = 'drupal'
    
    def db_for_read(self, model, **hints):
        if model._meta.app_label == self.app_name:
            return self.database_name
        return None
    
    def db_for_write(self, model, **hints):
        if model._meta.app_label == self.app_name:
            return self.database_name
        return None
    
    def allow_relation(self, obj1, obj2, **hints):
        """
        If either model is in this app but both are not, reject.
        """
        if obj1._meta.app_label == self.app_name or \
            obj2._meta.app_label == self.app_name:
            if obj1._meta.app_label == obj2._meta.app_label:
                return True
            else:
                return False
        return None
    
    def allow_syncdb(self, db, model):
        if db == self.database_name or model._meta.app_label == self.app_name:
            return False
        return None
