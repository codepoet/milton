# -*- coding: utf-8 -*-

import datetime
import logging
import re
from optparse import make_option

import pytz

from django.conf import settings
from django.db.models import Q
from django.db.utils import IntegrityError
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand, CommandError

from milton.drupal_support.models import *
from milton.models import *
from milton.templatetags.slugify import slugify


def vancode2int(vancode):
    if len(vancode):
        result = int(vancode[1:], 36)
    else:
        result = None
    return result

def _educate_date(d):
    if d.tzinfo == None:
        d = pytz.utc.localize(d)
    else:
        d = d.replace(tzinfo=pytz.utc)
    return d

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
    #     make_option('--format', default='json', dest='format', help='Specifies the output serialization format for fixtures.'),
    #     make_option('--indent', default=None, dest='indent', type='int', help='Specifies the indent level to use when pretty-printing output'),
    #     make_option('-e', '--exclude', dest='exclude',action='append', default=[], help='App to exclude (use multiple --exclude to exclude multiple apps).'),
    #     make_option('-v', '--verbose', dest='verbose', action="store", default="0", type="choice", choices=['0','1','2'], help='Verbose output'),
        make_option('-c','--comments', dest='comments', action="store_true", default=False, help="Import comments (default)."),
        make_option('-C','--no-comments', dest='comments', action="store_false", default=False, help="Do not import comments."),
        make_option('-s','--site-id', dest='site_id', action="store", help="Site ID to import records to."),
        make_option('-u','--site-url', dest='site_url', action="store", help="Lookup an existing site by URL."),
        make_option('-b','--blog-section', dest='blog_section', action="store", default="Blog", help="Section name to put blog entries in (empty string for None)."),
        make_option('-t','--story-section', dest='story_section', action="store", default="Story", help="Section name to put stories in (empty string for None)."),
        make_option('-g','--global', dest='global', action="store_true", default=False, help="Create items globally instead of site-specific, when possible (currently Sections and Resources)."),
        make_option(None,'--db-table', dest='db_table', action="store", help="Magic."),
        
    )
    help = 'Convert Drupal 6 tables to SP objects. The tables must be in the same database as SP.'
    # args = '[appname ...]'
    
    def handle(self, *app_labels, **options):
        log = logging.getLogger("milton")
        
        import_comments = options.get('comments', False)
        show_traceback = options.get('traceback', False)
        verbose = int(options.get('verbosity', 0))
        debug = (verbose == 2)
        user = User.objects.get(pk=1)
        
        if debug:
            log.setLevel(logging.DEBUG)
        elif verbose:
            log.setLevel(logging.INFO)
        else:
            log.setLevel(logging.WARNING)
        
        log.info("Starting Drupal conversion.")
        log.debug("Importing data with %s as the content owner." % (user.username,))
        
        db_table = options.get('db_table')
        if db_table:
            settings.DATABASES['drupal']['NAME'] = db_table
            log.info("Using table %s" % (settings.DATABASES['drupal']['NAME'],))
        
        # Prefetch the destination site
        site_obj = None
        if options.get("site_id", None):
            site_obj = Site.objects.get(pk=options.get("site_id"))
        elif options.get("site_url", None):
            site_obj = Site.objects.get_site_for_URL(options.get("site_url"))
        
        if not site_obj:
            raise CommandError("No site specified. Use either --site-id or --site-url to specify a site for the import.")
        
        # Always filter in a way that allows site-specific resources as well as global resources.
        # However, create objects assigned to the site unless otherwise specified.
        site_filter = { 'site__in': [site_obj, None] }
        if options.get("globally"):
            site_arg = None
        else:
            site_arg = site_obj
        
        # Follow the rabbit
        # while site_obj.alias_for:
            # site_obj = site_obj.alias_for
        
        log.info("Using site %s" % (site_obj.name,))
        
        try:
            # Get a list of nodes together and filter to core nodes.
            node_list = DrupalNode.objects.filter(type__in=("blog", "story", "page")).order_by("nid")
            log.info("Found %d nodes." % len(node_list))
            
            if len(node_list) == 0:
                raise CommandError("No supported Drupal nodes found in the current database.")
            
            # Create the sections, if needed
            def _get_or_create_section(section_name, options):
                if len(section_name):
                    try:
                        section = Section.objects.get(name=section_name, **site_filter)
                        log.info("Found section %s: %s" % (section_name, section))
                
                    except Section.DoesNotExist:
                        args = {}
                        args['name'] = section_name
                        args['slug'] = slugify(section_name)
                        args['site'] = site_arg
                    
                        section = Section(**args)
                        section.save()
                        log.info("Created section %s." % (section_name,))
                    return section
                return None
            
            blog_section = _get_or_create_section(options.get("blog_section"), options)
            story_section = _get_or_create_section(options.get("story_section"), options)
            
            for node in node_list:
                # Our document object
                obj = None
                
                # See if we've imported this node before
                # FIXME: This will be hard to make respectful of the global attribute as any previous import
                #        that brought in a node with a similar ID will cause a pass. Perhaps we need to verify
                #        the target (blech)?
                node_aliases = Redirect.objects.filter(original='/node/%d' % node.nid, site=site_obj)
                if node_aliases.count() > 0:
                    log.info("Skipping node %d" % node.nid)
                    continue
                
                # We haven't imported this.  Let's begin...
                log.info("Processing node (%d) %s" % (node.nid, node.title))
                
                if node.type == "blog" or node.type == "story":
                    if node.type == "blog":
                        log.debug("Node is a blog.")
                        section = blog_section
                    else:
                        log.debug("Node is a story.")
                        section = story_section
                    
                    # Create an Story for the node
                    obj = Story(
                        user = user,
                        title = node.title,
                        date_created = _educate_date(datetime.datetime.fromtimestamp(node.created)),
                        date_modified = _educate_date(datetime.datetime.fromtimestamp(node.changed)),
                        date_published = _educate_date(datetime.datetime.fromtimestamp(node.created)),
                        slug = slugify(node.title),
                        allow_comments = node.comment,
                        status = node.status,
                        section = section,
                        content_format = FORMAT_TEXTILE,
                        )
                
                    # Set the current content value
                    try:
                        contents = node.current_revision.get_parsed_contents()
                    
                        obj.excerpt = contents['teaser']
                        obj.content = contents['body']
                    except DrupalNodeRevision.DoesNotExist as e:
                        log.warning("No body content found for %s (%d)" % (obj.title, obj.id))
                
                    # Add to the site
                    obj.site = site_arg
                    
                    # Save the story
                    try:
                        obj.save()
                    except IntegrityError as e:
                        log.exception(e)
                        log.warning("Properties: %r, %r, %r", obj.site.id, obj.date_published, obj.slug)
                        log.warning("Node already imported: %s", node.title)
                        continue
                    
                    # # Import terms as categories
                    # drupal_terms = node.current_revision.terms.all()
                    # term_names = [t.name for t in drupal_terms]
                    # categories = []
                    # for name in term_names:
                    #     try:
                    #         category = Category.objects.get(name=name)
                    #     except Category.DoesNotExist as e:
                    #         category = Category(name=name, slug=slugify(name), site=site_obj)
                    #         category.save()
                    #     categories.append(category)
                    # obj.categories = categories
                    # log.debug("Set categories to: %s" % (categories,) )
                    
                    # Import terms as tags
                    drupal_terms = node.current_revision.terms.all()
                    term_names = [t.name for t in drupal_terms]
                    obj.tags.set(*term_names)
                    log.debug("Set tags to: %s" % (term_names,) )
                    
                    # Ensure the modification date is proper
                    obj.date_modified = _educate_date(datetime.datetime.fromtimestamp(node.changed))
                    obj.save()
                    
                elif node.type == "page":
                    log.debug("Node is a page.")
                    
                    # Create an object for the node
                    obj = Page(
                        title = node.title,
                        date_created = _educate_date(datetime.datetime.fromtimestamp(node.created)),
                        date_modified = _educate_date(datetime.datetime.fromtimestamp(node.changed)),
                        date_published = _educate_date(datetime.datetime.fromtimestamp(node.created)),
                        status = node.status,
                        content_format = FORMAT_TEXTILE,
                        )
                    
                    # Set the current content value
                    try:
                        obj.content = node.current_revision.body
                    except DrupalNodeRevision.DoesNotExist as e:
                        log.warning("No body content found for %s (%d)" % (obj.title, obj.id))
                    
                    # Save
                    try:
                        obj.save()
                    except IntegrityError as e:
                        log.warning("Properties: %r, %r, %r", obj.site.id, obj.date_published, obj.slug)
                        log.warning("Node already imported: %s", node.title)
                        continue
                    
                    # Add to the current site
                    obj.site = site_arg
                    
                    # Find the most recent URL for this object and assign it
                    aliases = DrupalUrlAlias.objects.filter(src='node/%d' % node.nid).order_by('-pid')
                    if aliases.count():
                        log.debug("Page aliases: %s" % (aliases,))
                        obj.url = '/' + aliases[0].dst
                    
                    # Ensure the modification date is proper
                    obj.date_modified = _educate_date(datetime.datetime.fromtimestamp(node.changed))
                    obj.save()
                
                else:
                    # Bail if we don't have something to work on
                    log.info("Unsupported type:", node.type)
                    continue
                
                # Lookup any URL aliases for this node and create redirects
                aliases = DrupalUrlAlias.objects.filter(src='node/%d' % node.nid)
                
                # First, the core "node/1" links should still work
                r = Redirect(original="/node/%d" % node.nid, target_object=obj, site=site_obj)
                r.save()
                
                # Now we get any others that were made
                for alias in aliases:
                    dst = "/" + alias.dst
                    if obj.__class__ == Page and dst == obj.url: continue
                    try:
                        r = Redirect(original=dst, target_object=obj, site=site_obj)
                        r.save()
                        log.debug("Created redirect from %s" % (dst,))
                    except Exception as e:
                        log.info("Failed to create redirect from %s to %s :%s" % (dst, obj, e))
                        continue
                
                # Create a ContentHistory for each old body value
                if node.revisions.count() > 1:
                    for revision in node.revisions.all()[1:]:
                        date = _educate_date(datetime.datetime.fromtimestamp(revision.timestamp))
                        
                        contents = revision.get_parsed_contents()
                        
                        ch = ContentHistory(
                            date_created = date,
                            date_modified = date,
                            owner = obj,
                            field_name = "excerpt",
                            content = contents['teaser'],
                        )
                        ch.save()
                        
                        ch = ContentHistory(
                            date_created = date,
                            date_modified = date,
                            owner = obj,
                            field_name = "content",
                            content = contents['body'],
                        )
                        ch.save()
                        
                        log.debug("Added revision %s" % (ch.date_created,))
                
                # Bring over the comments ## DEPRECATED
                # if import_comments:
                #     comments = self.get_comments_for_node(node)
                # else:
                #     comments = []
                #
                # # log.debug("  Comments:", comments.count())
                # for comment in comments:
                #     log.debug("Importing comment: %s %s" % (comment.thread, comment.subject))
                #
                #     # To handle the nested comments:
                #     #     Break up the thread property: 01.01.01 -> [1,1,1]
                #     #     Pop off the last item, as that is the location of the current comment (which we cannot directly set)
                #     #     For each remaining level, get the right relationship in a loop:
                #     #         Set an object to the first listed comment.
                #     #         Pop that value off the array.
                #     #         If there're more items left, set the loop object to that child of the current object.
                #     #         When we run out of list items, we found the parent.
                #
                #     # 01.00.05.07/ to 01.00.05.07 to [01,00,05,07]
                #     parents = comment.thread[:-1].split('.')
                #     # [1,0,5,7]
                #     parents = map(vancode2int, parents)
                #     # [1,0,5]
                #     parents = parents[:-1]
                #     if len(parents):
                #         # [0,0,5]
                #         parents[0] = parents[0] - 1
                #
                #     log.debug("Parents: %s (%s)" % (parents, comment.thread))
                #
                #     c_obj = None
                #     if len(parents):
                #         ct = ContentType.objects.get_for_model(obj.__class__)
                #         obj_comments = Comment.objects.filter(content_type=ct, object_pk=str(obj.id)).order_by('submit_date')
                #         log.debug("Object comments (%d): %s" % (obj_comments.count(), obj_comments))
                #         log.debug("Parents (%d): %s" % (len(parents), parents))
                #
                #         try:
                #             c_obj = obj_comments[parents[0]]
                #         except IndexError as e:
                #             log.info("Parent of comment subtree not found.  This can happen if a thread has a deleted comment.")
                #         parents = parents[1:]
                #         while c_obj and len(parents):
                #             if c_obj.children.count() > parents[0]:
                #                 c_obj = c_obj.children.all()[parents[0]]
                #                 parents = parents[1:]
                #             else:
                #                 log.warning("Object has %d comments, but the parent should be item %d" % (c_obj.children.count(), parents[0]))
                #                 break
                #         if c_obj: log.debug("Suspected parent: %s" % (c_obj.title,))
                #
                #     # Actually create the comment now
                #     c = Comment(
                #         content_object = obj,
                #         title = unicode(comment.subject)[:200],
                #         ip_address = unicode(comment.hostname)[:15],
                #         user_name = unicode(comment.name)[:50],
                #         user_email = unicode(comment.mail)[:75],
                #         user_url = unicode(comment.homepage)[:200],
                #         submit_date = _educate_date(datetime.datetime.fromtimestamp(comment.timestamp)),
                #         comment = unicode(comment.comment),
                #         site = Site.objects.get_current(),
                #         is_public = (not comment.status),
                #     )
                #     c.save()
                #
                #     # Now that the comment exists, we can set the parent.
                #     c.parent = c_obj
                #     c.save()
                #
                #     log.info(" Imported comment (%d) %s" % (comment.cid, comment.subject))
                
                log.debug(" Finished node: %s (%s)" % (obj.title, obj.get_absolute_url()))
            
        except Exception as e:
            if show_traceback:
                raise
            raise CommandError("Error: %s" % e)
        
        log.info("Done.")
    
    def get_comments_for_node(self, node):
        '''
        This generally requires a "real" database like Postgresql or MySQL.  Sqlite does not support SUBSTRING.
        But, that's okay.  Drupal required MySQL anyway, so do the conversion on a copy of the DB there, then
        migrate to whatever else you want to use.
        '''
        raw_comments = DrupalComment.objects.db_manager('drupal').raw("SELECT * FROM comments WHERE nid = %s ORDER BY SUBSTRING(comments.thread, 1, (LENGTH(comments.thread) - 1))", [node.nid])
        comments = []
        for comment in raw_comments:
            comments.append(comment)
        return comments
