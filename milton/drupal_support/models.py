# -*- coding: utf-8 -*-

import re
from django.db import models
from django.contrib import admin

"""
An importer for Drupal 6 sites stored in SQL.

Note that this was created for the author's specific use case, is quite
unsupported for general use, and will eventually be completely removed.
"""

class DrupalComment(models.Model):
    cid = models.IntegerField(primary_key=True)
    pid = models.IntegerField(null=True, blank=True, default=0)
    nid = models.ForeignKey("DrupalNode", db_column="nid")
    uid = models.ForeignKey("DrupalUser", db_column="uid")
    subject = models.CharField(null=True, blank=True, max_length=192)
    comment = models.TextField(null=True, blank=True, default="")
    hostname = models.CharField(null=True, blank=True, max_length=384)
    timestamp = models.IntegerField(null=True, blank=True, default=0)
    status = models.IntegerField(null=True, blank=True, default=0)
    format = models.IntegerField(null=True, blank=True, default=0)
    thread = models.CharField(null=True, blank=True, max_length=765)
    name = models.CharField(null=True, blank=True, max_length=180)
    mail = models.CharField(null=True, blank=True, max_length=192)
    homepage = models.CharField(null=True, blank=True, max_length=765)
    class Meta:
        db_table = 'comments'
        managed = False

class DrupalNode(models.Model):
    nid = models.IntegerField(primary_key=True)
    type = models.CharField(max_length=96, default="")
    title = models.CharField(max_length=765, default="")
    uid = models.ForeignKey("DrupalUser", db_column="uid")
    status = models.IntegerField(default=0)
    created = models.IntegerField(default=0)
    changed = models.IntegerField(default=0)
    comment = models.IntegerField(default=0)
    promote = models.IntegerField(default=0)
    moderate = models.IntegerField(default=0)
    sticky = models.IntegerField(default=0)
    current_revision = models.ForeignKey("DrupalNodeRevision", db_column="vid")
    language = models.CharField(max_length=36, default="")
    tnid = models.IntegerField(default=0)
    translate = models.IntegerField(default=0)
    
    terms = models.ManyToManyField('DrupalTermData', through="DrupalTermNode", blank=True)

    class Meta:
        db_table = 'node'
        managed = False

class DrupalNodeRevision(models.Model):
    nid = models.ForeignKey("DrupalNode", db_column="nid", related_name="revisions")
    vid = models.IntegerField(primary_key=True, default=0)
    uid = models.ForeignKey("DrupalUser", db_column="uid")
    title = models.CharField(max_length=765, default="")
    body = models.TextField(default="")
    teaser = models.TextField(default="")
    timestamp = models.IntegerField(default=0)
    format = models.IntegerField(default=0)
    log = models.TextField(null=True, blank=True, default="")
    
    terms = models.ManyToManyField('DrupalTermData', through="DrupalTermNode", blank=True)
    
    class Meta:
        db_table = 'node_revisions'
        managed = False
    
    def get_parsed_contents(self):
        '''
        Drupal stores a teaser and a body, but the teaser is almost always a substr of the body.
        If it is, then we use them.  If not, we look for "<!-- break -->" to denote the teaser
        then clean up the body by replacing that with newlines.
        '''
        
        p_teaser = ""
        p_content = ""
        
        teaser_len = len(self.teaser)
        body_len = len(self.body)
        
        if teaser_len == body_len:
            p_content = self.body
        
        elif self.teaser == self.body[:teaser_len]:
            p_teaser = self.teaser
            p_content = self.body
        
        else:
            break_re = re.compile('(?P<before>.*?)<!--\s*break\s*-->(?P<after>.*)', re.S|re.I)
            results = break_re.search(self.body)
            
            if results is not None:
                p_teaser = results.group("before")
                p_content = results.group("before") + "\n\n" + results.group("after")
            else:
                p_content = self.body
        
        return {'teaser':p_teaser, 'body':p_content}

class DrupalUrlAlias(models.Model):
    pid = models.IntegerField(primary_key=True, default="")
    src = models.CharField(max_length=255, default="")
    dst = models.CharField(unique=True, max_length=255, default="")
    language = models.CharField(max_length=36, default="")

    def __str__(self):
        return "%s to %s" % (self.src, self.dst)

    class Meta:
        db_table = 'url_alias'
        managed = False

class DrupalUser(models.Model):
    uid = models.IntegerField(primary_key=True)
    name = models.CharField(null=True, unique=True, max_length=180, default="")
    pass_field = models.CharField(null=True, max_length=96, db_column='pass', default="") # Field renamed because it was a Python reserved word. Field name made lowercase.
    mail = models.CharField(null=True, max_length=192, blank=True, default="")
    mode = models.IntegerField(null=True, default=0)
    sort = models.IntegerField(null=True, blank=True, default=0)
    threshold = models.IntegerField(null=True, blank=True, default=0)
    theme = models.CharField(null=True, max_length=765, default="")
    signature = models.CharField(null=True, max_length=765, default="")
    created = models.IntegerField(null=True, default=0)
    access = models.IntegerField(null=True, default=0)
    status = models.IntegerField(null=True, default=0)
    timezone = models.CharField(null=True, max_length=24, blank=True, default="")
    language = models.CharField(null=True, max_length=36, default="")
    picture = models.CharField(null=True, max_length=765, default="")
    init = models.CharField(null=True, max_length=192, blank=True, default="")
    data = models.TextField(null=True, blank=True, default="")
    login = models.IntegerField(null=True, default=0)
    class Meta:
        db_table = 'users'
        managed = False

admin.site.register(DrupalComment)
admin.site.register(DrupalNode)
admin.site.register(DrupalNodeRevision)
admin.site.register(DrupalUrlAlias)
admin.site.register(DrupalUser)

class DrupalTermData(models.Model):
    '''The actual terms'''
    tid = models.IntegerField(primary_key=True)
    vid = models.IntegerField()
    name = models.CharField(max_length=765)
    description = models.TextField(blank=True)
    weight = models.IntegerField()

    class Meta:
        db_table = 'term_data'
        managed = False

    def __str__(self):
        return self.name

class DrupalTermHierarchy(models.Model):
    '''Term parents/children'''
    tid = models.ForeignKey(DrupalTermData, primary_key=True, related_name="tid_set", db_column="tid")
    parent = models.ForeignKey(DrupalTermData, primary_key=True, db_column="parent")
    class Meta:
        db_table = 'term_hierarchy'
        managed = False

class DrupalTermNode(models.Model):
    '''Mapping of terms to nodes'''
    nid = models.ForeignKey(DrupalNode, primary_key=True, db_column="nid")
    tid = models.ForeignKey(DrupalTermData, primary_key=True, db_column="tid")
    vid = models.ForeignKey(DrupalNodeRevision, db_column='vid')
    class Meta:
        db_table = 'term_node'
        managed = False

admin.site.register(DrupalTermData)
admin.site.register(DrupalTermHierarchy)
admin.site.register(DrupalTermNode)

class DrupalTermRelation(models.Model):
     tid1 = models.IntegerField(unique=True)
     tid2 = models.IntegerField()
     trid = models.IntegerField(primary_key=True)
     class Meta:
         db_table = 'term_relation'
         managed = False
admin.site.register(DrupalTermRelation)

class DrupalTermSynonym(models.Model):
     tid = models.IntegerField()
     name = models.CharField(max_length=765)
     tsid = models.IntegerField(primary_key=True)
     class Meta:
         db_table = 'term_synonym'
         managed = False
admin.site.register(DrupalTermSynonym)

class DrupalVocabulary(models.Model):
     vid = models.IntegerField(primary_key=True)
     name = models.CharField(max_length=765)
     description = models.TextField(blank=True)
     help = models.CharField(max_length=765)
     relations = models.IntegerField()
     hierarchy = models.IntegerField()
     multiple = models.IntegerField()
     required = models.IntegerField()
     weight = models.IntegerField()
     module = models.CharField(max_length=765)
     tags = models.IntegerField()
     class Meta:
         db_table = 'vocabulary'
         verbose_name_plural = 'drupal vocabularies'
         managed = False
admin.site.register(DrupalVocabulary)
