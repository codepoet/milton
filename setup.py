#!/usr/bin/env python
import os
from setuptools import setup, find_packages

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

setup(
    name = "django-milton",
    version = "0.9",
    
    author='Adam Knight',
    author_email='adam@movq.us',
    description='A simple, yet complete personal CMS.',
    license='MIT',
    keywords='django blog cms wordpress website webpage page',
    url='https://bitbucket.org/codepoet/milton',
    long_description=README,
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    
    packages = find_packages(),
    include_package_data = True,
    
    setup_requires = [ "setuptools_git >= 0.3", ],
    install_requires = [
      "Django>=2.1,<2.2",
      "lxml",
      "pytz",
      "python-dateutil",
      "django-taggit==0.24.0",
      "Markdown",
      "textile",
      "pyyaml",
    ],
)
